from sparse import HeapMatrix, ListMatrix
from numpy import random
import pytest, numpy as np

@pytest.mark.parametrize("MatrixType", [HeapMatrix, ListMatrix])    
class Test_LocMatrix:
    @pytest.fixture(scope='function', params=[(20, 20, 0)])
    def random_matrix(self, request):
        rows, columns, seed = request.param
        random.seed(seed)
        return random.randint(0, 2, (rows, columns))
    
    @pytest.fixture(scope='function', params=[(10, 10, 30, 0)])
    def random_matrix_pair(self, request, MatrixType):
        rows, columns, entries, seed = request.param
        random.seed(seed)
        where = np.unique(
            np.column_stack((
                random.randint(0, rows,    entries), 
                random.randint(0, columns, entries)
            )),
            axis=0
        )
        sparse_array = MatrixType.from_nonzero_entries(where, (rows, columns))
        np_array = np.zeros((rows, columns), dtype=int)
        np.add.at(np_array, tuple(where.T), 1)
        np_array %= 2
        return sparse_array, np_array
    
    def test_creation(self, random_matrix, MatrixType):
        matrix = random_matrix
        loc_matrix = MatrixType(matrix)
        assert np.array_equal(np.array(loc_matrix), matrix)
        loc_matrix = MatrixType.from_nonzero_entries(np.argwhere(matrix), matrix.shape)
        assert np.array_equal(np.argwhere(loc_matrix), np.argwhere(matrix))
        assert np.array_equal(np.array(loc_matrix), matrix)
    
    def test_consolidation(self, random_matrix_pair):
        unconsolidated, np_array = random_matrix_pair
        assert np.array_equal(unconsolidated, np_array)
        assert np.array_equal(np.array(unconsolidated), np_array)
        assert np.array_equal(unconsolidated, np_array)
    
    def test_pivot(self, random_matrix_pair):
        def pivot(nparray, column):
            return max(np.nonzero(nparray[:, column])[0], default=-1) 
        sparse_array, np_array = random_matrix_pair
        assert all(sparse_array.pivot(j) == pivot(np_array, j) for j in range(sparse_array.shape[1]))
        sparse_array.consolidate()
        assert all(sparse_array.pivot(j) == pivot(np_array, j) for j in range(sparse_array.shape[1]))
    
    def test_column_operation(self, random_matrix: np.ndarray, MatrixType):
        matrix = random_matrix
        loc_matrix = MatrixType(random_matrix)
        loc_matrix.column_operation(0, 1)
        matrix[:,1] += matrix[:,0]
        matrix[:,1] %= 2
        assert np.array_equal(loc_matrix, matrix)
    
    @pytest.mark.parametrize('matrix1', [pytest.lazy_fixture('random_matrix')])
    @pytest.mark.parametrize('matrix2', [pytest.lazy_fixture('random_matrix')])
    def test_matmul(self, matrix1, matrix2, MatrixType):
        assert np.array_equal(MatrixType(matrix1) @ MatrixType(matrix2), matrix1 @ matrix2 % 2)

    def test_getitem(self, random_matrix, MatrixType):
        N  = random_matrix
        M  = MatrixType(N)
        M_ = MatrixType(N)
        print(np.array(M))
        for j in range(M.shape[1]):
            assert np.array_equal(M[:,j], N[:,[j]])
            assert np.array_equal(M, M_)
        for i in range(M.shape[1]):
            assert np.array_equal(M[i,:], N[[i],:])
            assert np.array_equal(M, M_)
        assert all(np.array_equal(M[:,slice(0,j)], N[:,slice(0,j)]) for j in range(M.shape[1]))
        assert all(np.array_equal(M[slice(0,i),:], N[slice(0,i),:]) for i in range(M.shape[0]))
        assert all(np.array_equal(M[:,range(0,j)], N[:,range(0,j)]) for j in range(M.shape[1]))
        assert all(np.array_equal(M[range(0,i),:], N[range(0,i),:]) for i in range(M.shape[0]))
        assert all(np.array_equal(M[slice(i,None),:], N[slice(i,None),:]) for i in range(M.shape[0]))
    
    def test_setitem(self, random_matrix, MatrixType):
        M = MatrixType(random_matrix)
        N = random_matrix
        M[:,0] = M[:,1]
        N[:,0] = N[:,1]
        assert np.array_equal(M, random_matrix)
        M[:,[0,2]] = M[:,[1,3]]
        N[:,[0,2]] = N[:,[1,3]]
        assert np.array_equal(M, random_matrix)
        M[:,slice(0,2)] = M[:,[3,4]]
        N[:,slice(0,2)] = N[:,[3,4]]
    
    def test_conversion(self, random_matrix, MatrixType):
        L = random_matrix
        M = MatrixType(L)
        assert np.array_equal(HeapMatrix(M), L)
        assert np.array_equal(ListMatrix(M), L)
        assert np.array_equal(HeapMatrix(M), M)
        assert np.array_equal(ListMatrix(M), M)
        