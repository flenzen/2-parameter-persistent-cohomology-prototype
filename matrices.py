from __future__ import annotations
from copy import deepcopy, copy
from sparse import HeapMatrix, LocMatrix, LorMatrix
import numpy as np, itertools as it
import functools as ft, builtins
import itertools as it
import collections.abc
from utilities import colex_keys, lex_keys, is_colex_sorted, neg_inf, pos_inf

def join(g1, g2=None):
    if g2 is None:
        return ft.reduce(lambda vw, xy: (max(vw[0], xy[0]), max(vw[1], xy[1])), g1, (neg_inf, neg_inf))
    else:
        return max(g1[0], g2[0]), max(g1[1], g2[1])
def meet(g1, g2=None):
    if g2 is None:
        return ft.reduce(lambda vw, xy: (min(vw[0], xy[0]), min(vw[1], xy[1])), g1, (pos_inf, pos_inf))
    else:
        return min(g1[0], g2[0]), min(g1[1], g2[1])
        
def leq_tot(a, b):
    return a[0] <= b[0] and a[1] <= b[1]
def leq_colex(a, b):
    return a[1] < b[1] or a[1] == b[1] and a[0] <= b[0]

class GM:
    @staticmethod
    def zeros(row_grades, col_grades, MatrixType=HeapMatrix):
        return GM(MatrixType.zeros((len(row_grades), len(col_grades))), row_grades, col_grades)
    @staticmethod
    def ones(row_grades, col_grades, MatrixType=HeapMatrix):
        return GM(MatrixType.ones((len(row_grades), len(col_grades))), row_grades, col_grades)  
    @staticmethod
    def eye(row_grades, col_grades=None, MatrixType=HeapMatrix):
        if col_grades is None:
            col_grades = row_grades
        assert len(row_grades) == len(col_grades), "Matrix must be square."
        assert all(leq_tot(r, c) for r, c in zip(row_grades, col_grades)), "Grades give no well-defined graded unit matrix."
        return GM(MatrixType.eye(len(row_grades), len(col_grades)), row_grades, col_grades)
    def __init__(self, entries, row_grades, col_grades, not_well_defined=False):
        assert entries.shape == (len(row_grades), len(col_grades)),\
            f"Lengths {(len(row_grades), len(col_grades))} of grade arrays "\
            f"do not match matrix shape {entries.shape}."    
        self.shape      = entries.shape
        self.entries    = entries
        self.row_grades = np.array(row_grades, int, copy=False).reshape((-1,2))
        self.col_grades = np.array(col_grades, int, copy=False).reshape((-1,2))
    def __eq__(self, other: GM):
        return  np.array_equal(self.row_grades, other.row_grades) \
            and np.array_equal(self.col_grades, other.col_grades) \
            and np.array_equal(self.entries, other.entries)
    def __str__(self):
        return f"Graded {type(self.entries)} of shape {self.shape}"
    def __repr__(self):
        return str(self)
    def __matmul__(self, other: GM) -> GM:
        """Matrix multiplication."""
        assert np.array_equal(self.col_grades, other.row_grades),\
            "Matrices must have same degrees along the common axis."
        return GM(self.entries @ other.entries, self.row_grades, other.col_grades)
    def __rmul__(self, other: GM):
        return GM(other * self.entries, self.row_grades, self.col_grades)
    def __neg__(self):
        return GM(-self.entries, self.row_grades, self.col_grades)
    def __or__(self, other: GM):
        """ Horizontal concatenation."""
        assert np.array_equal(self.row_grades, other.row_grades),\
            "Juxtapositioned matrices must have same column grade."
        return GM(
            np.hstack((self.entries, other.entries)),
            self.row_grades,
            np.vstack((self.col_grades, other.col_grades))
        )
    def __floordiv__(self, other: GM):
        """ Vertical concatenation."""
        assert np.array_equal(self.col_grades, other.col_grades),\
            "Stacked matrices must have same column grades."
        return GM(
            np.vstack((self.entries, other.entries)),
            np.vstack((self.row_grades, other.row_grades)),
            self.col_grades
        )
    def __getitem__(self, t):
        return GM(self.entries[t], np.atleast_2d(self.row_grades[t[0]]), np.atleast_2d(self.col_grades[t[1]]))
    def D(self, same_entry_type=False):
        """ Returns the graded anti-transpose of the matrix:
            Transpose the matrix, exchange row and column grades, negate grades,
            and reverse the indexing on rows and columns.
            Negation of the grades is done to reverse the ordering in priority queues.
            Reversal of the indexing is done to keep the grades lexicographically
            ascending, if they are before. """
        matrix = GM(
            self.entries.T().flip() 
                if isinstance(self.entries, LocMatrix) or isinstance(self.entries, LorMatrix)
                else np.flip(self.entries.T) , 
            np.flip(-self.col_grades, axis=0),
            np.flip(-self.row_grades, axis=0)
        )
        if same_entry_type:
            return matrix.to_entry_type(type(self.entries))
        else:
            return matrix
    def is_well_defined(self):
        """ Verifies that `M` represents a morphism of graded modules.
            This is the case if an entry `M[i,j]` is non-zero only if
            `row_grades[i] <= col_grades[j]` in the total order. """
        return all(
                leq_tot(self.row_grades[i], self.col_grades[j])
                for i, j in np.argwhere(self.entries)
            )
    def where_not_well_defined(self):
            return [
                (i,j) 
                for i, j in np.argwhere(self.entries)
                if not leq_tot(self.row_grades[i], self.col_grades[j])
            ]
    def is_minimal(self):
        """ Checks if `M` represents a minimal part of a resolution.
            Returns true if no `i`, `j` exist such that `M[i,j] != 0`
            and `row_grades[i] == col_grades[j]`. """
        return all(
            leq_tot(self.row_grades[i], self.col_grades[j]) 
            and not np.array_equal(self.row_grades[i], self.col_grades[j])
            for i, j in np.argwhere(self.entries)
        )
    def local_pairs(self):
        return [(i,j) 
            for j, col_grade in enumerate(self.col_grades)
            for i, row_grade in enumerate(self.row_grades) 
            if (self.entries[i,j] != 0 and np.array_equal(row_grade, col_grade))
        ]
    def to_array(self):
        if isinstance(self.entries, np.ndarray):
            return self
        else:
            return GM(self.entries.to_array(), self.row_grades, self.col_grades)
    def to_entry_type(self, Type):
        return GM(Type(self.entries), self.row_grades, self.col_grades)
    @classmethod
    def block(cls, matrices):
        """ Combines the double array `matrices` of matrices (vertical outer,
            horizontal inner) to a block matrix. """
        col_grades = np.concatenate([matrix.col_grades for matrix in matrices[0]])
        row_grades = np.concatenate([row[0].row_grades for row in matrices])
        # All rows have the same col_grades
        assert all( 
            np.array_equal(col_grades, np.concatenate([matrix.col_grades for matrix in row]))
            for row in matrices[2:]
        )
        # In each row, all matrices have the same row_grades
        assert all( 
            all(np.array_equal(row[0].row_grades, matrix.row_grades) for matrix in row[2:])
            for row in matrices
        )
        entries = np.block([[matrix.entries for matrix in row] for row in matrices])
        return GM(entries, row_grades, col_grades)
    @classmethod
    def vstack(cls, matrices):
        """ Stacks graded matrices vertically. """
        return cls.block([[matrix] for matrix in matrices])
    @classmethod
    def hstack(cls, matrices):
        """ Juxtapositions graded matrices horizontally. """
        return cls.block([matrices])
    def lex_sorted(self):
        return self[lex_keys(self.row_grades), lex_keys(self.col_grades)]
    def colex_sorted(self):
        return self[colex_keys(self.row_grades), colex_keys(self.col_grades)]
    def flip_grades(self):
        return GM(
            self.entries, 
            np.flip(self.row_grades, axis=1),
            np.flip(self.col_grades, axis=1),
        ).colex_sorted()


def matrices_from_file(filename, transpose=False, descending=False, start=0, stop=None):
    """ Generator that yields boundary matrices from `filename` in ascending 
        or descending order. Start obtaining matrix number `start`. """
    with np.load(filename) as file:
        n = next((d for d in it.count() if f'D{d}' not in file.files), 0)
        if stop is None:
            stop = n if not descending else -1
        r = range(start, stop) if not descending else range(start, stop, -1)
        for d in r:
            builtins.print(f"Reading D{d+1} from {filename}")
            entries, row_grades, col_grades = file[f'D{d}'], file[f'rg{d}'], file[f'cg{d}']
            matrix = GM(HeapMatrix.from_nonzero_entries(entries, (len(row_grades), len(col_grades))), row_grades, col_grades)
            assert is_colex_sorted(matrix.row_grades)
            assert is_colex_sorted(matrix.col_grades)
            if transpose:
                matrix = matrix.D(True)
            yield matrix

