from calendar import c
from copy import deepcopy
import numpy as np, itertools as it
from implementation import kernel_2d_sparse
from kernel_routines_test import is_unique
from sparse import HeapMatrix, ListMatrix, LocMatrix, LorMatrix, heapified
from utilities import Duration, Profiler, ProgressIterator, SDuration, Silence, lex_sorted, print, colex_sorted, print, colex_keys, inverse_permutation, lex_keys, is_colex_sorted, is_lex_sorted, to_right, to_top
from factorisation import factor_GM
from heapq import heapify, heappush, heappop, heapreplace
from matrices import GM, join, leq_tot, meet
from minimization_routines import minimize_by_columns, minimize_by_rows
from utilities import neg_inf, pos_inf
from collections import Counter

@Duration("Reduce")
def reduce_ltr2(M: LocMatrix, C: LocMatrix=None, compute_inverse=False):
    V = HeapMatrix.eye(M.shape[1])
    if compute_inverse:
        W = HeapMatrix.eye(M.shape[1])
    pivots = np.full(M.shape[0], -1)
    if C is not None:
        with SDuration("Clear"):
            C.consolidate()
            cleared = []
            for j in range(C.shape[1]):
                i = C.pivot(j)
                M.erase_column(i)
                V[:,i] = C[:,j]
                cleared.append((i,j))
        print(f"Cleared: {len(cleared)}: columns")
    with SDuration("Reduce"):
        for j in range(M.shape[1]):
            while (i := M.pivot(j)) != -1:
                if (k := pivots[i]) != -1:
                    M.column_operation(k, j)
                    V.column_operation(k, j)
                    if compute_inverse:
                        heappush(W.data[j], -k)
                else:
                    pivots[i] = j
                    break
    print(f"Already reduced: {sum(1 for c in V.data if len(c) == 1)} of {M.shape[1]} columns")
    # with SDuration("Consolidation"):
    #     M.consolidate()
    #     V.consolidate()
    if compute_inverse:
        W = ListMatrix(W)
        if C is not None:
            with SDuration("Inverse for cleared columns:"):
                cleared.sort()
                for i, j in cleared:
                    W[:, i] = W @ C[:, j]
        return M, V, HeapMatrix(W)
    else:
        return M, V


@Silence()
def nullity(m):
    return np.count_nonzero(reduce_ltr2(deepcopy(m))[0].pivot_rows() == -1)

def np_nullity(m):
    return m.shape[1] - np.linalg.matrix_rank(m.dense())

def complete_reduction_matrix(M):
    """ From a reduced matrix `M`, compute a matrix `R` such that `M @ R` is
        completely reduced; i.e., every row in `M @ R` with a pivot contains
        precisely one non-zero entry. """
    assert is_unique(M.pivot_rows())
    R = HeapMatrix.eye(M.shape[1])
    p = M.pivot_cols()
    q = p != -1
    pt = p[q]
    Mt = M[q, :]
    for j in pt: # iterate columns of M in ascending order of pivots.
        for k in Mt.data[j][1:]: # pivot entries are consolidated.
            R.column_operation(pt[-k], j)
    return R
    Y = M @ R
    Y.consolidate()
    assert np.all(Y.pivot_rows() == M.pivot_rows())
    assert all(len(c) == 1 for c in Y[M.pivot_rows(), :].data)

# Used to keep intermediate results for clearing

# W1_C = None
# W2_C = None
# Used to propagate the reordering of the columns of M to the factorisation.
h = None
h_inv = None


def rel_cohomology_kernel_and_factorisation(D: GM, columns = None, no_kernel=False):
    assert is_colex_sorted(D.row_grades)
    lr = lex_keys(D.row_grades)
    lc = lex_keys(D.col_grades)
    # Construct block rows of matrix N to reduce. Matrix N is the block matrix [Π⁻¹D, 0, Π⁻¹ \\ 0, D, E].
    N1 = HeapMatrix.block([[D.entries[lr,lc],           HeapMatrix.zeros(D.shape),  HeapMatrix.eye(D.shape[0])[lr,:]]])
    N2 = HeapMatrix.block([[HeapMatrix.zeros(D.shape),  D.entries,                  HeapMatrix.eye(D.shape[0])      ]])

    # The first block row N1 has grades concentrated in (*, inf), the second block N2 in (inf, *).
    g1 = D.row_grades[lr,0]
    g2 = D.row_grades[ :,1]

    # If column indices have been provided, use only these columns of N.
    if columns is not None:
        N1 = N1[:,columns]
        N2 = N2[:,columns]
    else:
        columns = slice(None, None, None)
    
    W = HeapMatrix.eye(N1.shape[1]).T()
    
    p = np.full(N2.shape[0], -1)

    for j in range(N2.shape[1]):
        while (i := N2.pivot(j)) != -1:
            if p[i] == -1:
                p[i] = j
                break
            if N1.pivot(j) < N1.pivot(p[i]):
                p[i], j = j, p[i]
            N1.column_operation(p[i], j)
            N2.column_operation(p[i], j)
            W.row_operation(j, p[i])
            
    
    p = np.full(N1.shape[0], -1)
    for j in range(N1.shape[1]):
        while (i := N1.pivot(j)) != -1:
            if p[i] == -1:
                p[i] = j
                break
            if N2.pivot(j) < N2.pivot(p[i]):
                p[i], j = j, p[i]
            N1.column_operation(p[i], j)
            N2.column_operation(p[i], j)
            W.row_operation(j, p[i])
    
    # Nonzero-columns of N
    nzc = [j for j in range(N1.shape[1]) if N1.pivot(j) != -1 or N2.pivot(j) != -1]

    if no_kernel:
        kernel = None
    else:
        # The desired cohomology kernel is the matrix with the same row grades as the input matrix, and
        # the the lowest column grades that make the matrix well-defined (i.e., the join of the row grades
        # of the non-zero entries).
        N1 = N1[:,nzc]
        N2 = N2[:,nzc]
        N1.consolidate()
        N2.consolidate()
        kernel = GM(
            HeapMatrix.block([[N1], [N2]]),
            np.vstack((
                np.column_stack((g1, np.full(g1.shape, neg_inf))), 
                np.column_stack((np.full(g2.shape, neg_inf), g2))
            )),
            np.array([
                (
                    g1[i] if (i := N1.pivot(j)) != -1 else neg_inf, 
                    g2[i] if (i := N2.pivot(j)) != -1 else neg_inf
                )
                for j in range(N1.shape[1])
            ])
        )
        assert kernel.is_well_defined()

    # The factorisation matrix is given by the (left) inverse reduction matrix W. Row grades are the
    # column grades of the above kernel; column grades are the column grades of the original matrix N.
    W = W[nzc,:]
    W.consolidate()
    factorisation = GM(
        W, 
        kernel.col_grades, 
        np.vstack((
            to_top(D.col_grades[lc]), 
            to_right(D.col_grades), 
            D.row_grades
        ))[columns]
    )
    assert factorisation.is_well_defined()
    return kernel, factorisation


@SDuration("Kernel")
def rel_cohomology_kernel(graded_D: GM, clearing = (None, None)):
    """ Computes the kernel used for relative cohomology; i.e., the kernel of the matrix [DP, D], where
        D is the coboundary matrix (columns assumed in colex order), and P is the permutation that brings
        columns of D in lex order. 
        
        The routine first computes ker DP and ker D separately (using clearing), and afterwards computes
        the 'mixed' kernel elements. """
    C1, C2 = clearing
    D  = graded_D.entries
    G1 = graded_D.col_grades
    G2 = graded_D.row_grades
    dim2, dim1 = D.shape

    S1 = lex_keys(G1)
    S2 = lex_keys(G2)
    S1_inv = inverse_permutation(S1)
    S2_inv = inverse_permutation(S2)

    # Form and reduce matrices D, DP, PDP (using the matrices D, PDP from prev. dimension for clearing).
    with SDuration("Copy matrices"):
        D0 = deepcopy(D[:, S1])
        D1 = D[S2, S1]
        D2 = D 
    
    with SDuration("Reductions"):
        R0, V0 = reduce_ltr2(D0, C1)
        R1, _  = reduce_ltr2(D1, C1)
        R2, V2 = reduce_ltr2(D2, C2)
    with SDuration("Copy reduced matrices"):
        C1 =          R1[:, R1.pivot_rows() != -1]
        C2 = deepcopy(R2[:, R2.pivot_rows() != -1])

    # Form the matrices [DP, D] to be reduced further (mixed reduction). Build column degrees of that matrix:
    # First block DP has degrees (*, inf), block D has degrees (inf, *).
    M = HeapMatrix.block([[R0, R2]])
    g = np.vstack((G1[S1], G1))
    g[:dim1,1] = neg_inf
    g[dim1:,0] = neg_inf
    g = list(map(tuple, g))

    # Count columns j reduced to zero at the latest possible grade l[j] (given by the grade of the respective simplex)
    # through 'partner cancellation'.
    l = list(map(tuple, graded_D.col_grades))
    latest_reduction_grade_attained = 0

    # Keep the indices of columns of the kernel received (not received) by partner cancellation for later use.
    global partner_cancellation, no_partner_cancellation
    partner_cancellation = np.full(dim1, -1)

    with SDuration("Mixed reduction"):
        # Usual reduction scheme
        V = HeapMatrix.block([[V0, HeapMatrix.zeros((dim1, dim1))], [HeapMatrix.zeros((dim1, dim1)), V2]]) # Reduction matrix
        Q = [(g[j], j) for j in range(M.shape[1])] # Grades and columns to visit
        heapify(Q)
        p = np.full(M.shape[0], -1) # pivot row -> column
        kg = [] # kernel grades
        kc = [] # kernel columns (column indices of V)
        while Q:
            z, j = heappop(Q)
            # builtins.print(j, z)
            while (i := M.pivot(j)) != -1:
                if j >= dim1 and z == l[j-dim1]:
                    M.erase_column(j)
                    V.data[j] = [-j, -S1_inv[j-dim1]]
                    latest_reduction_grade_attained += 1
                    partner_cancellation[j-dim1] = len(kg)
                    kg.append(z)
                    kc.append(j)
                    break
                elif (k := p[i]) == -1:
                    p[i] = j
                    break
                elif k >= j or not leq_tot(g[k], z):
                    heappush(Q, (join(g[k], z), k))
                    p[i] = j
                    break
                else:
                    assert j >= dim1
                    M.column_operation(k, j)
                    V.column_operation(k, j)
            else:
                kg.append(z)
                kc.append(j)
        K = GM(V[:, kc], g, kg)
    # print("Columns at (∞,*):", dim1)
    # print("- reduced to zero at (∞,*):", sum(1 for g in kg if g[0] == neg_inf))
    # print("- at latest possible grade:", latest_reduction_grade_attained)
    # print("- somewhere else:", dim1 - sum(1 for g in kg if g[0] == neg_inf) - latest_reduction_grade_attained)
    return K, (C1, C2)

def is_graded_kernel(M, V):
    assert M.shape[1] == V.shape[0]
    M.entries.consolidate()
    V.entries.consolidate()
    assert M.is_well_defined()
    assert V.is_well_defined()
    g = np.unique(np.array([join(u, v) for u in V.col_grades for v in V.col_grades]), axis=0)
    g = g[colex_keys(g)]
    assert np.array_equal(V.col_grades, np.array([join(V.row_grades[-i] for i in column) for column in V.entries.data]))
    assert not (M @ V).entries.any()
    for z in ProgressIterator(g[::-1]):
        valid_cols_V = np.all(V.col_grades <= z, axis=1)
        valid_cols_M = np.all(M.col_grades <= z, axis=1)
        valid_rows_M = np.all(M.row_grades <= z, axis=1)
        M_t = M.entries[:           , valid_cols_M]
        V_t = V.entries[valid_cols_M, valid_cols_V]

        assert is_unique(V_t.pivot_rows())
        assert not (M_t @ V_t).any()
        assert nullity(M_t) == V_t.shape[1]

def rel_cohomology_main(matrices):
    """ Cohomology computation via the three steps kernel, factorisation, minimisation. 
        `matrices` is supposed to be a generator of (graded) coboundary matrices with colex sorted rows/columns."""
    matrices = peekable(matrices)
    # In dimension zero, prepare boundary matrix for reduced cohomology.
    # D1 = [1 \\ ... \\ 1], with grade at -inf
    # K1 = [1 \\ 1], with grade at -inf
    D2 = matrices.peek()
    D1 = GM(HeapMatrix.ones((D2.shape[1], 1)), D2.col_grades, np.full((1, 2), pos_inf))
    assert not (D2 @ D1).entries.any()
    K1 = GM(HeapMatrix.ones((2, 1)), np.vstack((to_top(D1.col_grades), to_right(D1.col_grades))), np.full((1, 2), pos_inf))
    clearing = (None, None)
    for d, D2 in enumerate(matrices):
        dim1, dim0 = D1.shape
        S1, S0 = lex_keys(D1.row_grades), lex_keys(D1.col_grades)
        
        K2, clearing = rel_cohomology_kernel(deepcopy(D2), clearing)

        N = GM.block([
                [D1[S1,S0],                                  GM.zeros(D1.row_grades[S1], D1.col_grades)  , GM.eye(D1.row_grades)[S1,:]],
                [GM.zeros(D1.row_grades, D1.col_grades[S0]), D1                                          , GM.eye(D1.row_grades)      ],
            ])
        N.col_grades[:dim0,1]      = neg_inf
        N.col_grades[dim0:2*dim0,0]= neg_inf
        N.row_grades[:dim1,1]      = neg_inf
        N.row_grades[dim1:,0]      = neg_inf

        # region New kernel/factorisation approach
        # kernel, factorisation = rel_cohomology_kernel_and_factorisation(D1, keys[nlr])
        # keys0 = lex_keys(kernel.col_grades)
        # kernel = kernel[:, keys0]
        # factorisation = factorisation[keys0, :]
        # assert Counter(map(tuple, K2.col_grades)) == Counter(map(tuple, kernel.col_grades))
        # assert not (M @ kernel).entries.any()
        # assert kernel @ factorisation.to_entry_type(HeapMatrix) == N
        # endregion

        if __debug__:
            # Check kernel
            M = GM.block([[deepcopy(D2)[:, S1], deepcopy(D2)]])
            M.col_grades[:dim1, 1] = neg_inf
            M.col_grades[dim1:, 0] = neg_inf
            M.row_grades[:,:]      = neg_inf
            assert M.is_well_defined()
            assert not (M @ K2).entries.any()

        L_fac = GM.block([
                [K1],
                [GM(HeapMatrix.block([[HeapMatrix.zeros((dim1,dim0)), D1.entries]]) @ K1.entries, D2.col_grades, K1.col_grades)]
            ])
        use_pc_shortcut = True
        if use_pc_shortcut:
            # Determine columns of N and rows of N_fac that will be local, due to pc.
            pc_columns_N    = 2*dim0 + np.where(partner_cancellation != -1)[0]
            keep_columns_N  = np.setdiff1d(np.arange(N.shape[1]), pc_columns_N)
            pc_rows_N_fac   = partner_cancellation[partner_cancellation != -1]
            keep_rows_N_fac = np.setdiff1d(np.arange(K2.shape[1]), pc_rows_N_fac)
            # Truncate and minimize L, truncate, factor N, truncate and minimize N_fac
            L_fac           = L_fac[keep_columns_N, :]
            N               = N[:, keep_columns_N]
            keys            = lex_keys(L_fac.row_grades)
            L_fac           = L_fac[keys, :]
            L_min, nlr, nlc = minimize_by_columns(L_fac)
            N               = N[:, keys[nlr]]
            N_fac           = factor_GM(N, K2)[keep_rows_N_fac, :]
            N_min, nlr, nlc = minimize_by_columns(N_fac)
            L_min           = L_min[nlc, :]
        else:
            # Minimize L, truncate, factor and minimize N
            keys            = lex_keys(L_fac.row_grades)
            L_fac           = L_fac[keys,:]
            L_min, nlr, nlc = minimize_by_columns(L_fac)
            N               = N[:,keys[nlr]]
            N_fac           = factor_GM(N, K2)
            N_min, _, nlc   = minimize_by_columns(N_fac)
            L_min           = L_min[nlc,:]
        assert not (N_min @ L_min).entries.any()
        yield (N_min, L_min)
        D1, K1, dim0 = D2, K2, dim1

from more_itertools import peekable
def rel_cohomology_main_new(matrices):
    matrices = peekable(matrices)
    # Prepare 0th matrices for 
    # D1 = [1 \\ ... \\ 1], with grade at -inf
    # K1 = [1 \\ 1], with grade at -inf
    D2 = matrices.peek()
    D1 = GM(HeapMatrix.ones((D2.shape[1], 1)), D2.col_grades, np.full((1, 2), pos_inf))
    K1 = GM(HeapMatrix.ones((2, 1)), np.vstack((to_top(D1.col_grades), to_right(D1.col_grades))), np.full((1, 2), pos_inf))
    matrices.prepend(D1)

    for d, D1 in enumerate(matrices):
        print(f"{d}-cohomology (relative)")
        dim1, dim0 = D1.shape
        S0, S1 = lex_keys(D1.col_grades), lex_keys(D1.row_grades)
        if __debug__:
            N = GM.block([
                [D1[S1,S0],                                  GM.zeros(D1.row_grades[S1], D1.col_grades)  , GM.eye(D1.row_grades)[S1,:]],
                [GM.zeros(D1.row_grades, D1.col_grades[S0]), D1                                          , GM.eye(D1.row_grades)      ],
            ])
            N.col_grades[:dim0,1]      = neg_inf
            N.col_grades[dim0:2*dim0,0]= neg_inf
            N.row_grades[:dim1,1]      = neg_inf
            N.row_grades[dim1:,0]      = neg_inf
        
        with SDuration("Build L"):
            L_fac = GM.block([[K1], [GM(HeapMatrix.block([[HeapMatrix.zeros((dim1,dim0)), D1.entries]]) @ K1.entries, D1.row_grades, K1.col_grades)]])
            keys = lex_keys(L_fac.row_grades)
            L_fac = L_fac[keys,:]
        with SDuration("Minimize L"):
            L_fac_min, nlr, nlc = minimize_by_columns(L_fac)
        
        with SDuration("New kernel/factorisation routine"):
            kernel, factorisation = rel_cohomology_kernel_and_factorisation(deepcopy(D1), keys[nlr])
            keys0 = lex_keys(kernel.col_grades)
            kernel = kernel[:, keys0]
            factorisation = factorisation[keys0, :]

        if __debug__:
            assert kernel.shape[1] == dim1 + np.count_nonzero(kernel.col_grades[:,1] == neg_inf)
            assert not (N[:,keys[nlr]] @ L_fac_min).entries.any()
            assert kernel @ factorisation.to_entry_type(HeapMatrix) == N[:,keys[nlr]]
            assert not (factorisation.to_entry_type(HeapMatrix) @ L_fac_min).entries.any()

        with SDuration("Minimize N"):
            N_fac_min, _, nlc = minimize_by_rows(factorisation)
            L_fac_min = L_fac_min[nlc,:]
        yield (N_fac_min.to_entry_type(HeapMatrix), L_fac_min)
        D1, K1, dim0 = D1, kernel, dim1