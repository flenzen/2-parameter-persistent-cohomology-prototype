from contextlib import suppress
from math import inf
from copy import deepcopy
import heapq, numpy as np, itertools as it, collections.abc, abc, operator as op
from typing import List
from numba import njit, jit
from galois import GF

from utilities import SDuration, has_duplicates

def oddly_occurring(iterable):
    """ Returns a generator of the oddly occurring elements from its input. Same entries of 
        `sorted_iterable` must occur consecutively. """
    iterator = iter(iterable)
    j = next(iterator, None)
    while j is not None:
        k = next(iterator, None)
        if j == k:
            j = next(iterator, None)
        else:
            yield j
            j = k
    if j is not None:
        yield j

def symmetric_difference(*iterables):
    """ Returns a generator that yields the symmetric difference of the arguments.
        Arguments need to have negative entries and must be ascendingly sorted. """
    if len(iterables) == 0:
        return []
    if len(iterables) == 1:
        return iterables[0]
    return list(oddly_occurring(heapq.merge(*iterables)))

def intersection_parity(l1, l2):
    ''' Returns True iff the number of common elements in `l1`, `l2` is odd. 
        Both need to be either ascendingly sorted.'''
    i2 = iter(l2)
    p = False
    with suppress(StopIteration):
        v2 = next(l2)
        for v1 in l1:
            if v1 > v2:
                v2 = next(v2 for v2 in l2 if v2 >= v1)
            if v1 == v2:
                p = not p

def renumerate(iterable):
    i = len(iterable)
    for v in reversed(iterable):
        i -= 1
        yield i, v

from heapq import heapify, heappushpop, heappop, heappush
def pivot_ipq(a):
    if len(a) == 0:
        return 1, None, None
    i, p, q = heappop(a)
    while len(a) > 0 and i == a[0][0]:
        if len(q) > p+1:
            i, p, q = heappushpop(a, (q[p+1], p+1, q))
        else:
            i, p, q = heappop(a)
        if len(q) > p+1:
            i, p, q = heappushpop(a, (q[p+1], p+1, q))
        elif len(a) > 0:
            i, p, q = heappop(a)
        else:
            return 1, None, None
    return i, p, q

def insert_ipq(a, p, q):
    if len(q) > p:
        heappush(a, (q[p], p, q))

def eval_ipq(a, p, q):
    return list(symmetric_difference(q[p:], *[q[p:] for _, p, q in a]))

handled_functions = {}
def implements(numpy_function):
    """Register an __array_function__ implementation for MyArray objects."""
    def decorator(f):
        handled_functions[numpy_function] = f
        return f
    return decorator

@implements(np.nonzero)
def nonzero(array):
    return tuple(array.argwhere().T)
@implements(np.argwhere)
def argwhere(array):
    return array.argwhere()
@implements(np.count_nonzero)
def count_nonzero(array, axis=None):
    return array.count_nonzero(array, axis)
@implements(np.array_equal)
def array_equal(array_a, array_b):
    if isinstance(array_a, LocMatrix) and isinstance(array_b, LocMatrix):
        return array_a == array_b
    else:
        return np.array_equal(np.argwhere(array_a), np.argwhere(array_b))

def heapified(array):
    heapq.heapify(array)
    return array

class LocMatrix(metaclass=abc.ABCMeta):
    @classmethod
    @abc.abstractmethod
    def eye(cls, m, n=None, k=0): pass
    @classmethod
    @abc.abstractmethod
    def from_column_entries(cls, entries, shape): pass
    @classmethod
    def block(cls, matrices):
        def all_equal(iterable):
            g = it.groupby(iterable)
            return next(g, True) and not next(g, False)
        assert all_equal(sum(matrix.shape[1] for matrix in row) for row in matrices),\
            "All rows need to have the same number of columns."
        assert all(all_equal(matrix.shape[0] for matrix in row) for row in matrices),\
            "In each row, all matrices have the same number of rows"
        assert all(all(isinstance(matrix, cls) for matrix in row) for row in matrices),\
            f"Can only concatenate {cls} instances."
        return (
            sum(row[0].shape[0] for row in matrices),
            sum(matrix.shape[1] for matrix in matrices[0])
        )
    __array_priority__ = 1.0 
    @abc.abstractmethod
    def __array__(self): pass
    def __array_function__(self, func, types, args, kwargs):
        if func == np.block:
            return self.__class__.block(*args, **kwargs)
        if func == np.hstack:
            return self.__class__.block([args[0]])
        if func not in handled_functions:
            return NotImplemented
        return handled_functions[func](*args, **kwargs)
    def __eq__(self, other):
        if isinstance(other, LocMatrix):
            return self.shape == other.shape and np.array_equal(self.argwhere(), other.argwhere())
        else:
            return NotImplemented
    @abc.abstractmethod
    def __matmul__(self, other): pass
    def __rmatmul__(self, other):
        if isinstance(other, np.ndarray):
            return np.column_stack([
                np.sum(np.column_stack([(other.T)[k] for k in self.entries(column_index)]), axis=1) 
                for column_index in range(self.shape[1])
            ]) % 2
        else:
            return NotImplemented
    @abc.abstractmethod
    def __getitem__(self, index): pass
    @abc.abstractmethod
    def __setitem__(self, index, value): pass 
    @abc.abstractmethod
    def add_to_column(self, to_column, other, from_column): pass
    @abc.abstractmethod
    def add_at(self, index): pass
    def any(self):
        for j, column in enumerate(self.data):
            self.consolidate(j)
            if len(column) > 0:
                return True
        return False
    @abc.abstractmethod
    def argwhere(self): pass
    def consolidate(self, s=slice(None), stats=False):
        """ Sums all entries belonging to the same row index in the heap representing 
            the `j`-th column, such that there is at most one entry for each row index
            in that column afterwards."""
        if stats:
            print("before:", self.stats(), end='; ')
        for j in np.atleast_1d(range(self.shape[1])[s]):
            self.consolidate_column(j)
        if stats:
            print("after:", self.stats())
        return self
    def sort(self):
        for c in self.data:
            c.sort()
    @abc.abstractmethod
    def consolidate_column(self, j): pass
    def column_operation(self, from_column, to_column):
        assert from_column != to_column
        self.add_to_column(to_column, self, from_column)
    def count_nonzero(self, axis=None):
        self.consolidate()
        if axis is None:
            return sum(map(len, self.data))
        elif axis == 0:
            return [len(col) for col in self.data]
        elif axis == 1:
            return NotImplemented
    def erase_column(self, column_index):
        self.data[column_index].clear()
    @abc.abstractmethod
    def entries(self, j=slice(None, None, None)):
        """ Returns the nonzero row indices of column `j` in descending order.
            If `j` is an integer, a single list is returned; if `j` is an iterable of integers,
            a list of lists is returned."""
        raise NotImplementedError
    @abc.abstractmethod
    def extend(self, other): pass
    @abc.abstractmethod
    def pivot(self, j=0):
        """ Returns the row index of the column `j`, or –1 if that column is empty.
            The respective column will be consolidated as much as needed to tell the pivot. """
        pass
    def pivot_rows(self):
        return np.array([self.pivot(j) for j in range(self.shape[1])], int)
    def pivot_cols(self):
        pivots = np.full(self.shape[0], -1)
        pivots[self.pivot_rows()] = np.arange(self.shape[1])
        return pivots
    def __repr__(self):
        return f"{type(self).__name__} of shape {self.shape}"
    def T(self, preserve_type=False):
        return LorMatrix.from_transpose(self)
    def flip(self):
        return self[range(self.shape[0]-1, -1, -1), range(self.shape[1]-1, -1, -1)]
    def to_lor_matrix(self):
        return self.__class__(self.T()).T()

class HeapMatrix(LocMatrix):
    @classmethod
    def eye(cls, m, n=None, k=0):
        if n is None: n = m
        return cls([[-i+k] if 0 <= i-k < m else [] for i in range(n)], (m, n))
    @classmethod
    def zeros(cls, shape):
        return cls([[] for _ in range(shape[1])], shape, consolidated=True)
    @classmethod
    def ones(cls, shape):
        return cls([[-i for i in range(shape[0]-1, -1, -1)] for _ in range(shape[1])], shape, consolidated=True)
    @classmethod
    def from_column_entries(cls, entries, shape):
        return cls([heapified([-i for i in column]) for column in entries], shape)
    @classmethod
    def from_columnwise_delta(cls, array, shape):
        return cls([[-i] for i in array], shape)
    @classmethod
    def from_nonzero_entries(cls, entries, shape):
        data = [[] for _ in range(shape[1])]
        for i, j in entries:
            heapq.heappush(data[j], -i)        
        return cls(data, shape)
    @classmethod
    def block(cls, matrices):
        assert all(isinstance(r, list) and all(isinstance(m, HeapMatrix) for m in r) for r in matrices),\
            "Block matrix must be provided as list of lists of matrices (row major)."
        shape = LocMatrix.block(matrices)
        if len(matrices) == 1:
            return HeapMatrix(list(it.chain.from_iterable(map(op.attrgetter("data"), matrices[0]))), shape)
        data = [[] for _ in range(shape[1])]
        h = 0
        for row in matrices:
            for j, column in enumerate(it.chain.from_iterable(matrix.data for matrix in row)):
                data[j].extend([k-h for k in column])
            h += row[0].shape[0]
        for column in data: 
            heapq.heapify(column)
        # chain all consolidation matrices horizontally. Apply `all` vertically.
        consolidated = np.fromiter(map(all, zip(*(it.chain.from_iterable(m.consolidated for m in r) for r in matrices))), dtype=bool)

        return cls(data, shape, consolidated)    
    def __init__(self, from_object=None, shape=None, consolidated=False):
        """ Constructs a column sparse matrix that represents columns as heaps.
            `from_object` may be a numpy array, a `LorMatrix`,
            or a mere list of the column entries. In this case, `shape` has to be provided;
            in all other cases, shape must not be provided."""
        if isinstance(from_object, np.ndarray) and from_object.dtype == int and shape is None:
            self.shape   = from_object.shape
            self.data    = [(-np.nonzero(column % 2)[0]).tolist() for column in from_object.T]
            for column in self.data:
                heapq.heapify(column)
            if isinstance(consolidated, bool):
                self.consolidated = [consolidated for _ in range(self.shape[1])]
            elif isinstance(consolidated, collections.abc.Collection):
                assert len(self.data) == len(consolidated)
                self.consolidated = consolidated
            else:
                raise ValueError()
        elif isinstance(from_object, HeapMatrix) and shape is None:
            self.shape = from_object.shape
            self.data = from_object.data
            self.consolidated = from_object.consolidated
        elif isinstance(from_object, ListMatrix):
            self.shape = from_object.shape
            self.data = from_object.data
            self.consolidated = np.full(self.shape[1], True)
        elif isinstance(from_object, LorMatrix):
            self.data = [[] for _ in range(from_object.shape[1])]
            for i, j in from_object.argwhere():
                self.data[j].append(-i)
            for c in self.data:
                heapq.heapify(c)
            self.shape = from_object.shape
            self.consolidated = [False for _ in range(self.shape[1])]
        elif isinstance(from_object, list) and shape is not None:
            assert shape[1] == -1 or shape[1] == len(from_object)
            #assert all(k <= 0 for column in from_object for k in column),\
            #    "Must pass only nonpositive entries."
            self.shape   = shape[0], len(from_object)
            self.data    = from_object
            if isinstance(consolidated, bool):
                self.consolidated = [consolidated for _ in range(self.shape[1])]
            elif isinstance(consolidated, collections.abc.Collection):
                assert len(self.data) == len(consolidated)
                self.consolidated = consolidated
            else:
                raise ValueError()
        else:
            raise NotImplementedError
    def __add__(self, other):
        assert self.shape == other.shape
        assert isinstance(other, HeapMatrix)
        return HeapMatrix(
            [heapified(c1 + c2) for c1, c2 in zip(self.data, other.data)],
            self.shape
        )
    def __array__(self):
        entries = np.zeros(self.shape, dtype=int)
        np.add.at(entries, tuple(zip(*((-i, j) for j, column in enumerate(self.data) for i in column))), 1)
        entries %= 2
        return entries
    def __eq__(self, other):
        if self.shape != other.shape:
            raise ValueError("Can only compare matrices of same shape.")
        if isinstance(other, HeapMatrix):
            self.consolidate()
            other.consolidate()
            return all(set(c1) == set(c2) for c1, c2 in zip(self.data, other.data))
            return self.shape == other.shape and np.array_equal(self.argwhere(), other.argwhere())
        else:
            return NotImplemented
    def __getitem__(self, index):
        """ Returns an item or a submatrix (minor). `index` must consist of row and column index,
            each of which can be an integer index, a list or numpy array of rows/columns to select,
            or a slice including `:`. Row selection is expensive, column selection is cheap."""
        row_indices, col_indices = index
        
        # Column slicing
        if isinstance(col_indices, (int, np.integer)):
            data = [self.data[col_indices]]
            consolidated = [self.consolidated[col_indices]]
        elif isinstance(col_indices, slice):
            data = self.data[col_indices]
            consolidated = self.consolidated[col_indices]
        elif isinstance(col_indices, np.ndarray) and col_indices.dtype == bool:
            assert col_indices.shape == (self.shape[1],), "Shape mismatch"
            data         = [c for k, c in zip(col_indices, self.data) if k]
            consolidated = [c for k, c in zip(col_indices, self.consolidated) if k]
        elif isinstance(col_indices, collections.abc.Iterable):
        #elif isinstance(col_indices, np.ndarray) and col_indices.dtype == int:
            data = [self.data[j] for j in col_indices]
            consolidated = [self.consolidated[j] for j in col_indices]
        else:
            raise NotImplementedError
        
        # Row slicing
        if isinstance(row_indices, (int, np.integer)):
            assert 0 <= row_indices < self.shape[0]
            return HeapMatrix(
                    [
                        heapified([0 for k in column if -k == row_indices])
                        for column in data
                    ],
                    (1, len(data)),
                    consolidated
                )
        if isinstance(row_indices, slice):
            if row_indices == slice(None):
                return HeapMatrix(data, (self.shape[0], len(data)))
            # Convert other slices to iterables and process further.
            row_indices = range(self.shape[0])[row_indices]
            row_indices = np.array(row_indices)
        if isinstance(row_indices, np.ndarray) and row_indices.dtype == bool:
            row_indices = np.where(row_indices)[0]
        if isinstance(row_indices, collections.abc.Container):
            # Build assignment old row index -> new row index; then convert row indices accordingly.
            assert len(row_indices) == 0 or 0 <= min(row_indices) <= max(row_indices) < self.shape[0]
            old2new = np.full(self.shape[0], -1)
            old2new[row_indices] = np.arange(len(row_indices))
            data = [heapified([-old2new[-k] for k in column if old2new[-k] != -1]) for column in data]
            shape = (len(row_indices), len(data))
            return HeapMatrix(data, shape, consolidated)
        return NotImplemented
    def __setitem__(self, index, value):
        """ Only implemented for index[0] = : and value a LocMatrixF2. """
        if index[0] != slice(None) or not isinstance(value, HeapMatrix):
            return NotImplemented
        if isinstance(index[1], (int, np.integer)):
            self.data[index[1]] = value.data[0]
            self.consolidated[index[1]] = value.consolidated[0]
        elif isinstance(index[1], slice):
            self.data[index[1]] = value.data
            self.consolidated[index[1]] = value.consolidated
        elif isinstance(index[1], collections.abc.Iterable):
            for i, j in enumerate(index[1]):
                self.data[j] = value.data[i]
                self.consolidated[j] = value.consolidated[i]
        else:
            return NotImplemented
    def __matmul__(self, other):
        assert self.shape[1] == other.shape[0], "Shapes of input matrices don't match."
        if isinstance(other, LocMatrix):
            return HeapMatrix(
                    [
                        heapified(list(it.chain.from_iterable(self.data[-k] for k in c))) 
                        for c in other.data
                    ], 
                    (self.shape[0], other.shape[1])
                )
        elif isinstance(other, np.ndarray):
            return HeapMatrix([
                        heapified(list(it.chain.from_iterable(self.data[k] for k in column.nonzero()[0])))
                        for column in other.T % 2
                    ], 
                    (self.shape[0], other.shape[1])
                )
        else:
            return NotImplemented
    def add_to_column(self, to_column: int, other, from_column: int):
        """ Add `from_column`-th column of `other` to own `to_column`-th column. """
        for k in other.data[from_column]:
            heapq.heappush(self.data[to_column], k)
        self.consolidated[to_column] = False
    def add_at(self, index):
        if len(index)==2 and isinstance(index[0], (int, np.integer)) and isinstance(index[0], (int, np.integer)):
            assert index[0]<=self.shape[0] and index[1]<=self.shape[1],\
                "Index out of bounds."
            heapq.heappush(self.data[index[1]], -index[0])
            self.consolidated[index[1]] = False
        else:
            for i in index:
                self.add_at(i)
        return self
    def argwhere(self):
        self.consolidate()
        result = np.array([
            (-k, j) for j, col in enumerate(self.data) for k in col
        ]).reshape((-1, 2))
        return result[np.lexsort(np.flip(result.T, axis=0))]
    def consolidate_column(self, j):
        """ Eliminates entries for the same row index from the heap for column `j`. 
            The column is linearly ordered afterwards. """
        column = self.data[j]
        if len(column) <= 1:# or self.consolidated[j]:
            return
        new_column = []
        while len(column) >= 3:
            if column[0] == column[1] or column[0] == column[2]:
                heapq.heappop(column)
                heapq.heappop(column)
            else:
                heapq.heappush(new_column, heapq.heappop(column))
        if len(column) == 2:
            if column[0] == column[1]:
                heapq.heappop(column)
                heapq.heappop(column)
            else:
                heapq.heappush(new_column, heapq.heappop(column))
                heapq.heappush(new_column, heapq.heappop(column))
        elif len(column) == 1:
            heapq.heappush(new_column, heapq.heappop(column))
        self.data[j][:] = new_column
        self.consolidated[j] = True
    def delete_columns(self, index):
        del self.data[index]
        keep = np.full(self.shape[1], True)
        keep[index] = False
        self.consolidated = self.consolidated[keep]
        self.shape = (self.shape[0], len(self.data))
    def entries(self, j=slice(None, None, None), descending=True):
        self.consolidate(j)
        if isinstance(j, (int, np.integer)):
            return sorted(map(op.neg, self.data[j]), reverse=descending)
        elif isinstance(j, slice):
            return [sorted(map(op.neg, c), reverse=descending) for c in self.data[j]]
        elif isinstance(j, (collections.abc.Iterable)):
            return [sorted(map(op.neg, self.data[k]), reverse=descending) for k in j]
        else:
            return NotImplemented
    def extend(self, other):
        assert isinstance(other, HeapMatrix)
        assert self.shape[0] == other.shape[0]
        self.data.extend(other.data)
        self.consolidated.extend(other.consolidated)
        self.shape = (self.shape[0], self.shape[1] + other.shape[1])
    def extend_rows(self, new_rows):
        if new_rows < self.shape[0]:
            raise ValueError
        self.shape = (new_rows, self.shape[1])
    def pivot(self, j=0):
        """ Returns the largest row index of a non-zero entry the `j`-th column, 
            and the value of the entry. As a side effect, the pivot entry is consolidated. """
        column = self.data[j]
        # if self.consolidated[j]:
        #     if len(column) == 0:
        #         return -1
        #     else:
        #         return -column[0]
        while len(column) >= 3:
            if column[0] == column[1] or column[0] == column[2]:
                heapq.heappop(column)
                heapq.heappop(column)
            else:
                return -column[0]
        if len(column) == 2:
            if column[0] == column[1]:
                heapq.heappop(column)
                heapq.heappop(column)
                return -1
            else:
                return -column[0]
        elif len(column) == 1:
            return -column[0]
        else:
            return -1
    def pop_pivot(self, j):
        self.pivot(j)
        return -heapq.heappop(self.data[j])
    def reindex_rows(self, assignment, n_rows):
        """ Returns a martix in which the entry `i, j` is replaced by `assignment[i], j`.
            Performing `W.reindex_rows(a, m)` is equivalent to ` HeapMatrix.eye(m)[:, a] @ W`. 
            """
        assert len(assignment) == self.shape[0] and max(assignment) < n_rows
        data = [heapified([-assignment[-k] for k in c]) for c in self.data]
        return HeapMatrix(data, (n_rows, self.shape[1]))
    def reindex_columns(self, assignment, n_cols):
        """ Performing `W.reindex_cols(a, n)` is equivalent to `W @ HeapMatrix.eye(n)[a, :]`. 
            """
        assert len(assignment) == self.shape[1] and max(assignment) < n_cols
        p = np.full(n_cols, -1)
        p[assignment] = np.arange(len(assignment))
        data = [[] if p[j] == -1 else self.data[p[j]] for j in range(n_cols)]
        return HeapMatrix(data, (self.shape[0], n_cols))
    def dense(self):
        k = GF(2)
        r = k.Zeros(self.shape)
        for j, column in enumerate(self.data):
            for i in column:
                r[-i, j] += k(1)
        return r
    def stats(self):
        n_entries = sum(len(c) for c in self.data)
        density = n_entries / (self.shape[0] * self.shape[1])
        return f"Shape: {self.shape}, # entries: {n_entries}, density: {density:.4%}"

class ListMatrix(LocMatrix):
    """ Implements matrices as a list of columns and columns as lists of the nonzero entries.
        For compatibility with `HeapMatrix` instances and the min-heaps from `heapq`, `ListMatrix.data` stores
        the negated row indices of the nonzero entries. The (negated) indices are sorted ascendingly;
        i.e., the (negated) pivot comes first in the lists. The columns are always consolidated."""
    @classmethod
    def eye(cls, m, n=None, k=0):
        if n is None: n = m
        return cls([[-(i-k)] if 0 <= i-k < m else [] for i in range(n)], (m, n))
    @classmethod
    def zeros(cls, shape):
        return cls([[] for _ in range(shape[1])], shape, consolidated=True)
    @classmethod
    def from_column_entries(cls, entries, shape):
        """ Builds a `ListMatrix` a list of lists of its nonzero `entries`.  The lists
            are sorted and consolidated.  Entries are supposed to be negative. """
        def consolidate_list(ls):
            if len(ls) == 0:
                return ls
            ls.sort()
            r = [ls[0]]
            for i in ls[1:]:
                if r[-1] != i:
                    r.append(i)
                else:
                    r.pop()
            return r
        return cls([consolidate_list(column) for column in entries], shape)
    @classmethod
    def from_nonzero_entries(cls, entries, shape):
        data = [[] for _ in range(shape[1])]
        for i, j in entries:
            data[j].append(-i)
        for column in data:
            column.sort()
        return cls(data, shape)
    @classmethod
    def block(cls, matrices):
        shape = LocMatrix.block(matrices)
        data = [[] for _ in range(shape[1])]
        h = 0
        for row in matrices:
            for j, column in enumerate(it.chain.from_iterable(matrix.data for matrix in row)):
                data[j].extend(k+h for k in column.entries())
            h += row[0].shape[0]
        for column in data: 
            heapq.heapify(column)
        consolidated = np.array(list(map(
                all, 
                zip(*(it.chain(matrix.consolidated for matrix in row) for row in row))
            )))
        return cls(data, shape, consolidated)
    def __init__(self, from_object=None, shape=None): 
        """ Constructs a column sparse matrix that represents columns as heaps.
            `from_object` may be a numpy array, another ``, a `LorMatrix`,
            or a mere list of the column entries. In the latter case, the lists in `from_object` are
            heapified. In this case, `shape` has to be provided; in all other cases, shape must not
            be provided."""
        if isinstance(from_object, np.ndarray) and from_object.dtype == np.dtype('int') and shape is None:
            # Get row indices entries of nonzero entries, negate, and sort ascendingly.
            self.shape   = from_object.shape
            self.data    = [np.flip(-np.nonzero(column % 2)[0]).tolist() for column in from_object.T]
        elif isinstance(from_object, ListMatrix) and shape is None:
            # Share the same underlying data.
            self.shape = from_object.shape
            self.data = from_object.data
        elif isinstance(from_object, HeapMatrix):
            # A sorted heap is still a heap, so we can sort in place.
            from_object.consolidate()
            for column in from_object.data:
                column.sort()
            # Share the same underlying data.
            self.shape = from_object.shape
            self.data = from_object.data
        elif isinstance(from_object, LorMatrix):
            self.shape = from_object.shape[0], from_object.shape[1]
            self.data = [[] for _ in range(self.shape[1])]
            transpose = ListMatrix(from_object.T())
            for i, r in renumerate(transpose.data):
                for j in r:
                    self.data[-j].append(-i)
        elif isinstance(from_object, list) and shape is not None:
            assert shape[1] in (-1, len(from_object))
            self.shape   = shape[0], len(from_object)
            self.data    = from_object
        else:
            return self.__init__(HeapMatrix(from_object, shape))
    def __add__(self, other):
        assert isinstance(other, ListMatrix) and self.shape == other.shape, "Summands must have same shape"
        return ListMatrix(list(map(symmetric_difference, self.data, other.data)), self.shape)
    def __array__(self):
        entries = np.zeros(self.shape, dtype=int)
        np.add.at(entries, tuple(zip(*((-i, j) for j, column in enumerate(self.data) for i in column))), 1)
        entries %= 2
        return entries
    def __getitem__(self, index):
        """ Returns an item or a submatrix (minor). `index` must consist of row and column index,
            each of which can be an integer index, a list or numpy array of rows/columns to select,
            or a slice including `:`. Row selection is expensive, column selection is cheap."""
        row_indices, col_indices = index
        
        # Column slicing
        if isinstance(col_indices, (int, np.integer)):
            data = [self.data[col_indices]]
        elif isinstance(col_indices, slice):
            data = self.data[col_indices]
        elif isinstance(col_indices, collections.abc.Iterable):
            data = [self.data[j] for j in col_indices]
        else:
            return NotImplemented
        
        # Row slicing
        if isinstance(row_indices, (int, np.integer)):
            assert 0 <= row_indices < self.shape[0]
            return ListMatrix(
                    [[0 for k in column if -k == row_indices] for column in data],
                    (1, len(data))
                )
        if isinstance(row_indices, slice):
            if row_indices == slice(None):
                return ListMatrix(data, (self.shape[0], len(data)))
            # Convert other slices to iterables and process further.
            row_indices = range(self.shape[0])[row_indices]
        if isinstance(row_indices, collections.abc.Container):
            # Build assignment old row index -> new row index.
            # Then convert row indices accordingly.
            assert 0 <= min(row_indices, default=0) <= max(row_indices, default=0) < self.shape[0]
            assert not has_duplicates(row_indices)
            old2new = np.full(self.shape[0], -1)
            old2new[row_indices] = np.arange(len(row_indices))
            return ListMatrix(
                [sorted([-old2new[-k] for k in column if old2new[-k] != -1]) for column in data], 
                (len(row_indices), len(data))
            )
        return NotImplemented
    def __setitem__(self, index, value):
        """ Only implemented for index[0] = : and value a ListMatrix. """
        if index[0] != slice(None) or not isinstance(value, ListMatrix):
            return NotImplemented
        if isinstance(index[1], (int, np.integer)):
            self.data[index[1]] = value.data[0]
        elif isinstance(index[1], slice):
            self.data[index[1]] = value.data
        elif isinstance(index[1], collections.abc.Iterable):
            for i, j in enumerate(index[1]):
                self.data[j] = value.data[i]
        else:
            return NotImplemented
    def __matmul__(self, other):
        assert self.shape[1] == other.shape[0], "Shapes of input matrices don't match."
        if isinstance(other, LocMatrix):
            return ListMatrix(
                [symmetric_difference(*(self.data[-k] for k in c)) for c in other.data], 
                (self.shape[0], other.shape[1])
            )
        else:
            return NotImplemented
    def __rmatmul__(self, other):
        if isinstance(other, LorMatrix) and isinstance(other_T := other.T(), ListMatrix):
            return ListMatrix(
                [[-i for i, row in reversed(list(enumerate(other_T.data))) if intersection_parity(row, column)] 
                 for column in self.data],
                (other.shape[0], self.shape[1])
            )
        return NotImplemented
    def add_to_column(self, to_column: int, other, from_column: int):
        """ Add `from_column`-th column of `other` to own `to_column`-th column. """
        self.data[to_column] = list(symmetric_difference(self.data[to_column], other.data[from_column]))
    def add_at(self, index):
        return NotImplemented
    def argwhere(self):
        result = np.array([(-k, j) for j, col in enumerate(self.data) for k in col]).reshape((-1, 2))
        return result[np.lexsort(np.flip(result.T, axis=0))]
    def consolidate_column(self, j):
        pass
    def entries(self, j=slice(None, None, None), descending=True):
        if not descending:
            l = lambda c: list(map(op.neg, c))
        else:
            l = lambda c: list(map(op.neg, reversed(c)))
        if isinstance(j, (int, np.integer)):
            return l(self.data[j])
        elif isinstance(j, slice):
            return [l(c) for c in self.data[j]]
        elif isinstance(j, (collections.abc.Iterable)):
            return [l(self.data[j0]) for j0 in j]
        else:
            return NotImplemented    
    def extend(self, other):
        assert isinstance(other, HeapMatrix)
        assert self.shape[0] == other.shape[0]
        self.data.extend(other.data)
        self.shape = (self.shape[0], self.shape[1] + other.shape[1])
    def pivot(self, j=0):
        """ Returns the largest row index of a non-zero entry the `j`-th column, 
            and the value of the entry. """
        if len(self.data[j]) > 0:
            return -self.data[j][0]
        else:
            return -1
    def pop_pivot(self, j):
        p = self.pivot(j)
        del self.data[j][0]
        return p    
    def reindex_rows(self, assignment, rows):
        """ Changes the entry `i, j` to `assignment[i], j`. Assumes that `assignment` is injective. """
        data = [[-assignment[-k] for k in c] for c in self.data]
        return ListMatrix(data, (rows, self.shape[1]))

class LorMatrix:
    @staticmethod
    def from_transpose(T):
        assert isinstance(T, LocMatrix)
        M = LorMatrix(shape=(T.shape[1], T.shape[0]))
        M._T = T
        return M
    @staticmethod
    def from_array(array):
        return LorMatrix.from_transpose(LocMatrix(array.T))
    @staticmethod
    def eye(n, m=None, type=HeapMatrix):
        if m is None:
            m = n
        return LorMatrix.from_transpose(type.eye(m, n))
    def __init__(self, from_object=None, shape=None):
        if isinstance(from_object, (np.ndarray, LocMatrix, LorMatrix)):
            self._T = LocMatrix(from_object._T)
            self.shape = self._T.shape[1], self._T.shape[0]
        elif from_object is None:
            self._T = None
            self.shape = shape
        else: raise NotImplementedError
    def __array__(self):
        return np.array(self._T).T
    def __array_function__(self, func, types, args, kwargs):
        if func == np.block:
            return LorMatrix.block(*args, **kwargs)
        if func not in handled_functions:
            return NotImplemented
        return handled_functions[func](*args, **kwargs)
    def __getitem__(self, index):
        return LorMatrix.from_transpose(self._T[index[1], index[0]])
    def __matmul__(self, other):
        if isinstance(other, LorMatrix):
            return LorMatrix.from_transpose(other.T() @ self._T)
        else:
            return NotImplemented
    def row_operation(self, from_row, to_row):
        self._T.column_operation(from_row, to_row)
    def consolidate(self):
        self._T.consolidate()
    def count_nonzero(self):
        return self._T.count_nonzero()
    def any(self):
        return self._T.any()
    def argwhere(self):
        return np.fliplr(self._T.argwhere())
    def T(self):
        return self._T
    def flip(self):
        return LorMatrix.from_transpose(self._T.flip())

