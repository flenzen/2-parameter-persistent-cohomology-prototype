from heapq import heapify
import multiprocessing, copy, tempfile, builtins

# from py import builtin
from sparse import HeapMatrix, LocMatrix, LorMatrix, heapified, insert_ipq, pivot_ipq
from utilities import ProgressInfoIterator, Duration, SDuration, inverse_permutation, print
from kernel_routines import CohomologyKernelData
from matrices import GM
import numpy as np, time, heapq
def get_pivots(T):
    pivots = np.full(T.shape[0], -1)
    for j in range(T.shape[1]):
        i = T.pivot(j)
        assert i != -1 and pivots[i] == -1, "T is not reduced."
        pivots[i] = j
    return pivots

@SDuration("Factor")
def factor(M: LocMatrix, T: LocMatrix, pivots=None) -> HeapMatrix:
    ''' Return a matrix N such that M = TN. T must be reduced. If pivots is provided,
        `pivots` is supposed to be an array i -> column with pivot i of T. It is not
        checked that these are the actual pivots of T.
        Implements the factorisation through column additions from T to M.'''
    assert M.shape[0] == T.shape[0], "M and T need to have the same number of rows."
    if pivots is None:
        pivots = get_pivots(T)
    else:
        assert len(pivots) == T.shape[0], "Given pivots don't match number of rows of T."
    result = []
    for j in range(M.shape[1]):
        result.append([])
        while (i := M.pivot(j)) != -1:
            assert pivots[i] != -1, "M does not factor through T."
            M.add_to_column(j, T, pivots[i])
            result[-1].append(-pivots[i])
        heapq.heapify(result[-1])
    return HeapMatrix(result, (T.shape[1], M.shape[1]))

_pivots = None
_M = None
_T = None
def _factor_p_worker(j):
    result = []
    while (i := _M.pivot(j)) != -1:
        assert _pivots[i] != -1, "M does not factor through T."
        _M.add_to_column(j, _T, _pivots[i])
        result.append(-_pivots[i])
    heapify(result)
    return result

@SDuration("Factor (parallely)")
def factor_p(M: LocMatrix, T: LocMatrix, pivots=None) -> HeapMatrix:
    ''' Return a matrix N such that M = TN. T must be reduced. If pivots is provided,
        `pivots` is supposed to be an array i -> column with pivot i of T. It is not
        checked that these are the actual pivots of T.
        Implements the factorisation through column additions from T to M.'''
    assert M.shape[0] == T.shape[0], "M and T need to have the same number of rows."
    global _pivots, _M, _T
    if pivots is None:
        pivots = get_pivots(T)
    else:
        assert len(pivots) == T.shape[0], "Given pivots don't match number of rows of T."
    _pivots = pivots
    _M = M
    _T = T
    with multiprocessing.Pool(4) as pool:
        results = pool.map(_factor_p_worker, range(M.shape[1]), chunksize=M.shape[1]//32)
    N = HeapMatrix(results, (T.shape[1], M.shape[1]))
    N.consolidate()
    return N

@Duration("Factor (by pointers)")
def factor_ptr_heap(M: LocMatrix, T: LocMatrix, pivots=None):
    """ Like `by_column_operations`, but maintains a heap of active summands to the columns of M and T."""
    assert M.shape[0] == T.shape[0], "M and T need to have the same number of rows."
    M.consolidate()
    T.consolidate()
    TM = T.data + M.data
    if pivots is None:
        pivots = get_pivots(T)
    else:
        assert len(pivots) == T.shape[0], "Given pivots don't match number of rows of T."
    active_summands = []
    result = []
    with tempfile.NamedTemporaryFile('wt', delete=False) as f:#TODO remove
        print("Write timing to", f.name)
        builtins.print('time n_summands', file=f)
        for j in range(M.shape[1]):
            start = time.time()
            q = TM[T.shape[1] + j]
            result.append([])
            if len(q) == 0: continue
            p = 0
            i = q[0]
            while i != 1:
                # Realize column addition
                assert (k := pivots[-i]) != -1, "M does not factor"
                s = TM[k]
                result[-1].append(-k)
                insert_ipq(active_summands, p+1, q)
                insert_ipq(active_summands,   1, s)
                i, p, q = pivot_ipq(active_summands)
            heapify(result[-1])
            builtins.print(time.time() - start, len(result[-1]), file=f)#TODO remove
    return HeapMatrix(result, (T.shape[1], M.shape[1]))

def _factor_ptr_heap_p_worker(j):
    result = []
    q = _M[j]
    if len(q) == 0:
        return result
    p = 0
    i = q[0]
    active_summands = []
    while i != 1:
        # Realize column addition
        assert (k := _pivots[-i]) != -1, "M does not factor"
        s = _M[k]
        result.append(-k)
        insert_ipq(active_summands, p+1, q)
        insert_ipq(active_summands,   1, s)
        i, p, q = pivot_ipq(active_summands)
    heapify(result)
    return result

@Duration("Factor (parallelly by pointers)")
def factor_ptr_heap_p(M: LocMatrix, T: LocMatrix, pivots=None):
    """ Like `by_column_operations`, but maintains a heap of active summands to the columns of M and T."""
    assert M.shape[0] == T.shape[0], "M and T need to have the same number of rows."
    global _M, _pivots
    if pivots is None:
        pivots = get_pivots(T)
    else:
        assert len(pivots) == T.shape[0], "Given pivots don't match number of rows of T."
    M.consolidate()
    T.consolidate()
    _M = T.data + M.data
    _pivots = pivots
    with multiprocessing.Pool(4) as pool:
        results = pool.map(_factor_ptr_heap_p_worker, range(T.shape[1], T.shape[1]+M.shape[1]), M.shape[1]//32)
    N = HeapMatrix(results, (T.shape[1], M.shape[1]))
    return N

def factor_smartly_left(D2: HeapMatrix, K: CohomologyKernelData):
    """ D2: coboundary matrix Cⁱ → Cⁱ⁺¹ for computing Hⁱ.
        K:  result of `cohomology_kernel` from Hⁱ⁻¹."""
    dim0    = len(K.D_kernel_indices_inv)
    dim2, dim1 = D2.shape
    Ered    = K.V0[:, K.V0_Er_cols][dim0+dim1:, :]              # reduced E'-block in V0
    X       = HeapMatrix.zeros((dim2, K.V0.shape[1]))          # the (potentially) nonzero part of L̂
    X[:, K.V0_Er_cols] = D2 @ Ered
    L_fac   = HeapMatrix.block([[K.V0], [X]])
    return L_fac

def factor_GM(M: GM, T: GM, f=factor) -> GM:
    """ Factors graded matrix `M` through graded matrix `T`.  Employs the factorisation
        mechanism `f` (by default: indirection through heap of pointers). """
    return GM(f(M.entries, T.entries), T.col_grades, M.col_grades)

def factor_smartly_right(S1, S2, K0: CohomologyKernelData, K1: CohomologyKernelData, cols):
    """ Computes the factorisation `N̂` of `N` through `V̊`.  Employs knowledge of the
        specific shape s of V̊ and N.

        Arguments:
            S1 -- permutation rlex -> rcolex in dimension i
            S2 -- permutation rlex -> rcolex in dimension i+1
            K0 -- kernel data from dimension i-1
            K1 -- kernel data from dimension i
            cols -- columns of N to factor.

        Returns:
            `N̂[:,cols]` """
    with SDuration("Preparation"):
        dim0 = K0.W̃.shape[1]
        dim1 = len(S1)
        dim2 = len(S2)

        # Get P1 for which V0 @ P1 = D̃ᵈ
        P1 = HeapMatrix.from_columnwise_delta(
            K1.V0_pivot_cols[K0.D̃.pivot_rows()],
            (K1.V0.shape[1], K0.D̃.shape[1])
        )
        W1 = (P1 @ K0.W̃).data

        # Get P3 for which V0 @ P3 = D̃ᵈ⁺¹
        P3 = HeapMatrix.from_columnwise_delta(
            K1.V0_pivot_cols[dim1+dim2+K1.D̃.pivot_rows()],
            (K1.V0.shape[1], K1.D̃.shape[1])
        )
        W3 = (P3 @ K1.W̃).data

        # Comment to factor part C naively
        # Get P2 for which V0 @ P2 = D̰ᵈ⁺¹
        D̰ = K1.D̰
        W̰ = K1.W̰
        P2 = HeapMatrix.from_columnwise_delta(
            K1.V0_pivot_cols[dim1+D̰.pivot_rows()],
            (K1.V0.shape[1], D̰.shape[1])
        )
        W2 = (P2 @ W̰).data

        # R is the matrix for which V̊R is completely reduced (i.e., rows with a pivot have precisely one nonzero entry)
        R = K1.R

        D_indices = []
        D_targets = []

    with SDuration("Loop"):
        N_f_cols = []
        for j, k in enumerate(cols):
            # Compute the j-th column of N̂, which should be obtained by factoring N[:,k]
            if k < dim0:
                # N[:,k] is a column of [Dᵈ \\ 0 \\ 0], so N̂[:,k] is from W̃ᵈ.
                N_f_cols.append(W1[k])
            elif k < dim0+dim1:
                # N[:,k] is a column of [Eᵈ' \\ 'D'ᵈ⁺¹ \\ 0], so N̂[:,k] is from [R'ᵈ \\ 0 \\ W̰'ᵈ].
                if (v := K1.D̰_kernel_indices_inv[(l := S1[k-dim0])]) != -1:
                    N_f_cols.append(heapified(W2[l] + K1.D̰_R.data[v]))
                else:
                    N_f_cols.append(W2[l])
            elif k < dim0+2*dim1:
                # N[:,k] is a column of [Eᵈ \\ 0 \\ Dᵈ⁺¹], so N̂[:,k] is from [Rᵈ \\ 0 \\ W̃ᵈ].
                if (v := K1.D_kernel_indices_inv[(l := k-dim0-dim1)]) != -1: #
                    N_f_cols.append(heapified(W3[l] + K1.R.data[v]))
                else:
                    N_f_cols.append(W3[l])
            else:
                # N[:,k] is a column of [0\\E'ᵈ⁺¹\\Eᵈ⁺¹], need to factor to get N̂[:,k].
                N_f_cols.append(None)
                D_targets.append(j)
                D_indices.append(k-dim0-2*dim1)

    with Duration("Factor (fin,fin)-block"):
        # Build and factor column [0\\E'ᵈ⁺¹\\Eᵈ⁺¹] of N.
        S2_inv = inverse_permutation(S2)
        D = HeapMatrix(
                [[-dim1-dim2-k, -dim1-S2_inv[k]] for k in D_indices],
                (dim1+2*dim2, len(D_indices))
            )
        print("Nontrivially factoring columns:", D.shape[1])
        # D_fac = factor(copy.deepcopy(D), K1.V0, K1.V0_pivot_cols)
        # D_fac_2 = factor_p(copy.deepcopy(D), K1.V0, K1.V0_pivot_cols)
        D_fac = factor_ptr_heap(copy.deepcopy(D), K1.V0, K1.V0_pivot_cols)
        # D_fac_4 = factor_ptr_heap_p(copy.deepcopy(D), K1.V0, K1.V0_pivot_cols)
        for j, c in zip(D_targets, D_fac.data):
            N_f_cols[j] = c
    return HeapMatrix(N_f_cols, (K1.V0.shape[1], -1))
