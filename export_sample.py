#!/usr/bin/env python3
from math import comb
from cohomology_clearing_draft import matrices_from_file
from complexes import Simplices, boundary_matrices, coboundary_matrices, random_SO_matrices, random_annulus, random_clifford_torus, random_orthogonal_matrices, rank, vietoris_rips, random_sphere, unrank, transpose_grading
import numpy as np, itertools as it, matplotlib.pyplot as plt

from minimization_routines import minimize_ptr

def plot_sample_2d(coordinates, cplx, title):
    ax = plt.gca()
    cplx = iter(cplx)
    points, point_grades = next(cplx)
    ranked_edges, edge_grades = next(cplx)
    edges = np.array(list(map(lambda N: unrank(1, N), ranked_edges)))
    #ax.quiver(*coordinates[edges[:,0]].T, *(coordinates[edges[:,1]] - coordinates[edges[:,1]]).T)
    qu = ax.quiver(
            * coordinates[edges[:,0]].T, 
            *(coordinates[edges[:,1]] - coordinates[edges[:,0]]).T,
            edge_grades[:,0],
            cmap='winter',
            scale_units='xy', angles='xy', scale=1, headwidth=0, headlength=0
        )
    plt.colorbar(qu)
    sc = ax.scatter(*coordinates[points].T, 3, point_grades[:,1], cmap='spring')
    plt.colorbar(sc)
    plt.title(title)
    plt.show()

def save_cochain_complex(cplx, out_file):
    """ Saves the complex `cplx`, given as a list of ranked simplices and their grades,
        to `out_file`, in form of graded coboundary matrices.  """
    print(f"Saving complex to {out_file}")
    np.savez_compressed(out_file, **dict(it.chain.from_iterable(
            {
                f"D{d}": np.argwhere(D.entries),
                f"rg{d}": D.row_grades, 
                f"cg{d}": D.col_grades
            }.items()
            for d, D in enumerate(cplx)
        )))

import struct
def save_custom_format(filename, cplx):
    with open(filename, 'wb') as file:
        for D in cplx:
            bytes_written = file.write(struct.pack('I', 0)) # This is later overwritten with the number of bytes written.
            entries = D.entries.entries()
            bytes_written += file.write(struct.pack(
                    "2I"                    # For the (unsigned) shape of the matrix
                    f"{2*D.shape[0]}i"      # For the row_grades
                    f"{2*D.shape[1]}i",     # for the col_grades
                    *D.shape,
                    *it.chain.from_iterable(D.row_grades.tolist()),
                    *it.chain.from_iterable(D.col_grades.tolist())
            ))
            for column in entries:
                bytes_written += file.write(struct.pack(
                    "I"                      # For the number of entries in that column
                    f"{len(column)}I",      # The entries
                    len(column),
                    *column
                ))
            file.seek(-bytes_written, 2)
            file.write(struct.pack('I', bytes_written))
            file.seek(0, 2)
            print(bytes_written, "bytes written.")

def save_graded_complex(filename, cplx):
    with open(filename, 'wb') as file:
        for simplices, bigrades in cplx:
            assert len(simplices) == len(bigrades)
            file.write(struct.pack(
                'I' + len(simplices) + 'iiI',
                len(simplices),
                *it.chain.from_iterable((x, y, simplex) for simplex, (x, y) in zip(simplices, bigrades))
            ))


def read_simplicial_complex_file(filename):
    with open(filename) as input:
        d = 0
        graded_simplices = []
        for line in input:
            if len(line) == 0 or line[0] == '#':
                continue
            left, right = line.split(';')
            vertices    = tuple(map(int,    left.split()))
            grade       = tuple(map(float, right.split()))
            simplex     = rank(vertices)
            if len(vertices) != d+1:
                assert len(vertices) == d+2
                if d == 0:
                    N = len(graded_simplices)
                    print(f"In dimension {d}: {len(graded_simplices)} simplices.")
                else:
                    print(f"In dimension {d}: {len(graded_simplices)} simplices"
                          f"({len(graded_simplices) / comb(N, d+1):.0%} of full complex).")
                graded_simplices.sort(key=lambda x: (x[0][1], x[0][0], x[1]))
                grades, simplices = map(list, zip(*graded_simplices))
                yield simplices, np.array(grades)
                graded_simplices = []
                d += 1
            graded_simplices.append((grade, simplex))
        graded_simplices.sort(key=lambda x: (x[0][1], x[0][0], x[1]))
        grades, simplices = map(list, zip(*graded_simplices))
        yield simplices, np.array(grades)

def txt2npz(in_file, out_file):
    """ Converts samples from the format Alex Rolle uses into our format. """
    save_cochain_complex(coboundary_matrices(read_simplicial_complex_file(in_file)), out_file)

def minimize_cochain_complex(matrices):
    i = iter(matrices)
    D_min, nlr, nlc = minimize_ptr(next(i))
    for D in i:
        D1_min, nlr, nlc = minimize_ptr(D[:, nlr])
        yield D_min[nlc,:]
        D_min = D1_min
    yield D_min

def minimize_cochain_complex_file(in_file, out_file):
    save_cochain_complex(minimize_cochain_complex(matrices_from_file(in_file), out_file))

if __name__ == "__main__":
    #save_custom_format("samples/Alex.bin", coboundary_matrices(read_simplicial_complex_file("samples/Alex.txt")))
    n = 100
    np.random.seed(1)
    dim=2
    p = random_sphere(n, dim+1, 0.1)
    matrices = list(coboundary_matrices(vietoris_rips(p, max_dimen=4)))
    save_custom_format(f"samples/matrices_2-sphere-{n}.bin", matrices)
    #save_cochain_complex(matrices, f"samples/full_2-sphere_{n}_pts")
    exit(0)

    f = f"samples/full_2-sphere_{n}_pts_dim3"
    # p = random_orthogonal_matrices(n, 3, 0.05)
    # f = f"samples/full_O3_{n}_pts"
    # p = random_SO_matrices(n, 3, 0.05)
    # f = f"samples/full_SO3_{n}_pts"
    # print(f"Exporting to {f}.")
    c1, c2 = it.tee(coboundary_matrices(vietoris_rips(p, max_dimen=3)))
    c2 = (m.flip_grades() for m in c2)
    save_cochain_complex(c1, f + "_rips_density")
    save_cochain_complex(c2, f + "_density_rips")
    
