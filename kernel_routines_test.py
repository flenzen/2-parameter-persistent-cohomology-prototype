from copy import deepcopy
from dataclasses import dataclass
import pytest, numpy as np
from kernel_routines import reduce_ltr, cohomology_kernel_heap_lazy, cohomology_kernel_heap_lazy_D̰
from matrices import matrices_from_file
from sparse import HeapMatrix, ListMatrix
from utilities import inverse_permutation, is_colex_sorted, is_lex_sorted
 
def is_unique(array):
    return np.max(np.unique(array, return_counts=True)[1]) <= 1

def is_completely_reduced(M):
    """ Returns `True` if no two columns of `M` have the same pivot row (reduced),
        and in a row with a pivot, there is no other nonzero entry. """
    M.consolidate()
    pivot_rows = set(M.pivot_rows())
    for j in range(M.shape[1]):
        if len(set(M.entries(j)) & pivot_rows) > 1:
            return False
    return True

@pytest.mark.parametrize('clearing', [False, True], ids=['', 'clearing'])
def test_reduce_ltr(boundary_matrices, clearing):
    (_, D1, D2), *_ = boundary_matrices
    D1 = D1.entries
    D2 = D2.entries
    C = reduce_ltr(deepcopy(D1))[0] if clearing else None
    D̃, V, W, v0_indices, _, d̃_induces, _, _, R, _ = reduce_ltr(deepcopy(D2), C)
    V0 = V[:, v0_indices] 
    Ṽ  = V[:, d̃_induces] 

    assert is_unique(D̃.pivot_rows())
    assert is_unique(V.pivot_rows())
    assert not (D2 @ V0).any()
    assert D2 == D̃ @ W
    assert D̃ == D2 @ Ṽ
    assert D̃.shape[1] + V0.shape[1] == D2.shape[1]
    assert is_unique(R.pivot_rows()) and R.shape[0] == R.shape[1]
    assert is_completely_reduced(V0 @ R)
    
@pytest.fixture(params=[
    # pairs (filename, dimension)
    ("samples/full_2-sphere_10_pts_density_rips.npz", 1)
])
def boundary_matrices(request):
    filename, dimension = request.param
    D1, D2 = matrices_from_file(filename, start=dimension-1, stop=dimension+1)
    D = (None, D1, D2)
    G = (D1.col_grades, D1.row_grades, D2.row_grades)
    S = tuple(np.lexsort(np.flip(g.T, axis=0)) for g in G)
    S_inv = tuple(inverse_permutation(s) for s in S)

    assert all(is_colex_sorted(g) for g in G), "Input grades are not in colex order"
    assert all(is_lex_sorted(g[s]) for g, s in zip(G, S))
    
    return (D, S, S_inv, G)

@dataclass
class ResolutionMatrices:
    M: HeapMatrix
    N: HeapMatrix
    L: HeapMatrix
    U: HeapMatrix

def resolution_matrices_for_testing(boundary_matrices):
    """ Returns the matrices M, N, L that are the starting point for the computation
        of the cohomology between D[1] and D[2]. """
    D, S, S_inv, G = boundary_matrices
    _,    dim0 = D[1].shape
    dim2, dim1 = D[2].shape
    M = HeapMatrix.block([[D[2].entries, HeapMatrix.eye(dim2)[:,S[2]], HeapMatrix.eye(dim2)]])
    N = HeapMatrix.block([
        [D[1].entries,                      HeapMatrix.eye(dim1)[:,S[1]],      HeapMatrix.eye(dim1),              HeapMatrix.zeros((dim1, dim2)) ],
        [HeapMatrix.zeros((dim2, dim0)),   D[2].entries[S[2],S[1]],            HeapMatrix.zeros((dim2, dim1)),    HeapMatrix.eye(dim2)[S[2],:]   ],
        [HeapMatrix.zeros((dim2, dim0)),   HeapMatrix.zeros((dim2, dim1)),    D[2].entries,                       HeapMatrix.eye(dim2)           ]
    ])
    L = HeapMatrix.block([
        [                                   HeapMatrix.eye(dim0 + 2*dim1)                                          ],
        [HeapMatrix.zeros((dim2, dim0)),   D[2].entries[S[2], :][:, S[1]],     HeapMatrix.zeros((dim2, dim1))     ],
        [HeapMatrix.zeros((dim2, dim0)),   HeapMatrix.zeros((dim2, dim1)),    D[2].entries                        ],
    ])
    U = HeapMatrix.block([
        [HeapMatrix.eye(dim0 + 2*dim1), HeapMatrix.zeros((dim0+2*dim1, dim2))],
        [HeapMatrix.zeros((dim2, dim0+2*dim1)), HeapMatrix.eye(dim2)[S[2], :]],
        [HeapMatrix.zeros((dim2, dim0+2*dim1)), HeapMatrix.eye(dim2)]
    ])
    return ResolutionMatrices(M, N, L, U)

@pytest.fixture(params=[cohomology_kernel_heap_lazy_D̰])
def kernel_data(request, boundary_matrices):
    """ Provide `CohomologyKernelData`-objects `K0, K1` for two consecutive kernel computations. """
    D, S, S_inv, G = boundary_matrices
    kernel_function = request.param
    K0 = kernel_function(deepcopy(D[1]), S[1], S1=S[0])
    K1 = kernel_function(deepcopy(D[2]), S[2], K0.D̃, S1=S[1])
    #K1 = kernel_function(deepcopy(D[2]), S[2], S1=S[1])
    return K0, K1

class Test_cohomology_kernel:
    @pytest.mark.parametrize('kernel_data', [cohomology_kernel_heap_lazy, cohomology_kernel_heap_lazy_D̰], indirect=True, ids=[ 'lazy heap', 'lazy heap w/ D̰'])
    def test_kernel(self, boundary_matrices, kernel_data):
        # V0 is the kernel of (D, E', E).
        D, S, S_inv, G = boundary_matrices
        K1, K = kernel_data
        dim2, dim1 = D[2].shape
        R = resolution_matrices_for_testing(boundary_matrices)
        assert not (R.M @ K.V0).any()
        assert K.V0.shape[1] == dim1 + dim2
        assert is_unique(K.V0.pivot_rows())
        assert np.array_equal(K.V0_pivot_cols, K.V0.pivot_cols())
    
    @pytest.mark.parametrize('kernel_data', [cohomology_kernel_heap_lazy, cohomology_kernel_heap_lazy_D̰], indirect=True, ids=[ 'lazy heap', 'lazy heap w/ D̰'])
    def test_partner_cols(self, boundary_matrices, kernel_data):
        # V0 contains partner cancellation columns as described V0_partner_cols.
        D, S, S_inv, G = boundary_matrices
        K1, K = kernel_data
        dim2, dim1 = D[2].shape
        assert all(
            set(K.V0.entries(v))  == {dim1 + S_inv[2][i], dim1+dim2 + i}
            for i, v in enumerate(K.V0_partner_cols) if v != -1
        )
        assert all(
            (K.V0_col_grades[v], G[2][i] + (1,1))
            for i, v in enumerate(K.V0_partner_cols) if v != -1
        )
    
    @pytest.mark.parametrize('kernel_data', [cohomology_kernel_heap_lazy, cohomology_kernel_heap_lazy_D̰], indirect=True, ids=[ 'lazy heap', 'lazy heap w/ D̰'])
    def test_reduced_matrices(self, boundary_matrices, kernel_data):
        # D̃, W̃, Ṽ and D̰, W̰, V̰ relate as they should.
        D, S, S_inv, G = boundary_matrices
        _, K = kernel_data
        if K.D̃ is not None:
            assert K.D̃ == D[2].entries @ K.Ṽ
            assert K.D̃ @ K.W̃ == D[2].entries
        if K.D̰ is not None:
            assert K.D̰ == D[2][S[2],:].entries @ K.V̰
            assert K.D̰ @ K.W̰ == D[2][S[2],:].entries
        
    @pytest.mark.parametrize('kernel_data', [cohomology_kernel_heap_lazy_D̰], indirect=True)
    def test_data_for_smart_factorisation(self, boundary_matrices, kernel_data):
        # Test if V0 contains the submatrices as needed for ``
        # V0 contains D̃ and D̰ where it should.
        D, S, S_inv, G = boundary_matrices
        K0, K1 = kernel_data
        dim2, dim1 = D[2].shape
        rank_D2 = K1.W̃.shape[0]
        nullity_D2 = D[2].shape[1] - rank_D2

        # Check if the blocks (V̰ \\ D̰ \\ 0) and (Ṽ \\ 0 \\ D̃) satisfy M(...) = 0
        M  = HeapMatrix.block([[D[2].entries, HeapMatrix.eye(dim2)[:,S[2]], HeapMatrix.eye(dim2)]])
        U2 = HeapMatrix.block([[HeapMatrix(K1.V̰)], [K1.D̰], [HeapMatrix.zeros((dim2, rank_D2))]])
        U3 = HeapMatrix.block([[HeapMatrix(K1.Ṽ)], [HeapMatrix.zeros((dim2, rank_D2))], [K1.D̃]])
        assert not (M @ U2).any()
        assert not (M @ U3).any()        
        
        # Check if we find D̃ and D̰ as submatrices of V0.
        assert K0.D̃ == K1.V0[0:dim1, K1.V0_pivot_cols[K0.D̃.pivot_rows()]] # (clearing)
        assert K1.D̰ == K1.V0[dim1:dim1+dim2, K1.V0_pivot_cols[dim1 + K1.D̰.pivot_rows()]]
        assert K1.D̃ == K1.V0[dim1+dim2:dim1+2*dim2, K1.V0_pivot_cols[dim1+dim2+K1.D̃.pivot_rows()]]

        # Check if we find V̰ and Ṽ as submatrices of V0.
        assert K1.Ṽ == K1.V0[0:dim1, K1.V0_pivot_cols[dim1+dim2+K1.D̃.pivot_rows()]]
        assert K1.V̰ == K1.V0[0:dim1, K1.V0_pivot_cols[dim1 + K1.D̰.pivot_rows()]]

        # Check if we can reconstruct D1, D2 from V0.
        P1 = HeapMatrix.from_columnwise_delta(K1.V0_pivot_cols[K0.D̃.pivot_rows()], (K1.V0.shape[1], K0.D̃.shape[1]))
        assert K1.V0[:dim1,:] @ P1 @ K0.W̃ == D[1].entries

        P2 = HeapMatrix.from_columnwise_delta(K1.V0_pivot_cols[dim1+K1.D̰.pivot_rows()], (K1.V0.shape[1], K1.D̰.shape[1]))
        assert K1.V0[dim1:dim1+dim2, :] @ P2 @ K1.W̰ == D[2].entries[S[2],:]

        P3 = HeapMatrix.from_columnwise_delta(K1.V0_pivot_cols[dim1+dim2+K1.D̃.pivot_rows()], (K1.V0.shape[1], K1.D̃.shape[1]))
        assert K1.V0[dim1+dim2:, :] @ P3 @ K1.W̃ == D[2].entries

        # V0_D1 @ R should be completely reduced.
        V0_D2 = K1.V0[:dim1, :nullity_D2]
        assert is_completely_reduced(V0_D2 @ K1.R)

        # Column indices j such that D @ V[:,j] = 0
        D1_zero_columns = np.where(K1.D_kernel_indices_inv != -1)[0]
        # Column indices j such that D @ V[:,j] != 0
        D1_nonzero_columns = np.where(K1.D_kernel_indices_inv == -1)[0]     
        
        # Check if (E + ṼW̃)[:,j] = VR[:,k] or zero:
        X = ListMatrix.eye(dim1) + K1.Ṽ @ K1.W̃
        assert not X[:, D1_nonzero_columns].any()
        assert X[:, D1_zero_columns] == V0_D2 @ K1.R
        
        # X = ListMatrix.eye(dim1) + K1.V̰ @ K1.W̰
        # assert not X[:, K1.D̰_columns].any()
        # assert X[:, D1_zero_columns] == V0_D2 @ K1.R
 
        X = ListMatrix.eye(dim1) + K1.V̰ @ K1.W̰
        D̰_columns = np.where(K1.D̰_kernel_indices_inv == -1)[0]
        D̰_ker_columns = np.where(K1.D̰_kernel_indices_inv != -1)[0]
        assert not X[:, D̰_columns].any()
        assert X[:, D̰_ker_columns] == V0_D2 @ K1.D̰_R