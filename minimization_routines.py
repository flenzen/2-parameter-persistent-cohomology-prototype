from contextlib import suppress
from decimal import DivisionByZero
import itertools as it
from utilities import ProgressInfoIterator, is_non_decreasingly_sorted, print, sdiv, SDuration, Duration, Silence
from matrices import GM
from sparse import HeapMatrix, LocMatrix
from math import inf
import numpy as np

@SDuration("Minimize")
def minimize_by_columns(M: GM, first_phase_only=False):
    assert is_non_decreasingly_sorted(M.row_grades) and is_non_decreasingly_sorted(M.col_grades)

    A = M.entries
    column_grades, row_grades = M.col_grades, M.row_grades
    p = np.full(A.shape[0], -1)
    
    # Determine local columns
    lc = []
    nlc = []
    with SDuration("Phase I"):
        for j, grade in enumerate(M.col_grades):
            while (i := A.pivot(j)) != -1 and np.array_equal(row_grades[i], grade):
                if p[i] != -1:
                    A.column_operation(p[i], j)
                else:
                    A.consolidate(j)
                    p[i] = j
                    lc.append(j)
                    break
            else:
                nlc.append(j)
    with suppress(ZeroDivisionError):
        print(
            f"{M.shape[1]-len(nlc)} local pairs "
            f"({(M.shape[1]-len(nlc)) / M.shape[0]:.0%} of rows, "
            f"{sdiv((M.shape[1]-len(nlc)), M.shape[1]):.0%} of columns)"
        )
    
    # Get new indices of non-local rows 
    new_indices = np.full_like(p, -1)
    rows_nonlocal = (p == -1)
    new_indices[rows_nonlocal] = np.arange(np.count_nonzero(rows_nonlocal))
    nlr = np.where(rows_nonlocal)[0]
    nlc = np.array(nlc, dtype=int)

    if first_phase_only:
        return nlr, nlc 

    # Reduce non-local columns.
    new_columns = []
    with SDuration("Phase II"):
        for j in nlc:
            new_column = []
            while (i := A.pivot(j)) != -1:
                if p[i] == -1:
                    new_column.append(new_indices[i])
                    A.pop_pivot(j)
                else:
                    A.column_operation(p[i], j)
                assert A.pivot(j) < i
            new_columns.append(new_column)
    result = GM(
            HeapMatrix.from_column_entries(new_columns, (len(nlr), -1)),
            M.row_grades[nlr],
            M.col_grades[nlc]
        )
    return result, nlr, nlc

from sparse import insert_ipq, pivot_ipq, eval_ipq
def minimize_ptr(M: GM, first_phase_only=False):
    """ Minimizes the graded matrix `M` using an indirect heap of pointers to lists
        representing the columns M. """
    assert isinstance(M.entries, LocMatrix)
    M.entries.consolidate()
    A = M.entries.data

    # Determine local columns: Reduce the column until it becomes local. Write the
    # remainder back to the list.
    pivots = np.full(M.shape[0], -1)
    lc = []
    nlc = []
    with SDuration("Phase I"):
        for j, grade in enumerate(M.col_grades):
            q = A[j]
            if len(q) == 0: continue
            p = 0
            i = q[0]
            active_summands = []
            while i != 1 and np.array_equal(M.row_grades[-i], grade):
                if (v := pivots[-i]) != -1:
                    insert_ipq(active_summands, p+1, q)
                    insert_ipq(active_summands,   1, A[v])
                    i, p, q = pivot_ipq(active_summands)
                else:
                    pivots[-i] = j
                    lc.append(j)
                    break
            else:
                nlc.append(j)
            A[j] = eval_ipq(active_summands, p, q)
    
    # Get new indices of non-local rows 
    new_indices = np.full_like(pivots, -1)
    row_is_nonlocal = (pivots == -1)
    new_indices[row_is_nonlocal] = -np.arange(np.count_nonzero(row_is_nonlocal))
    nlr = np.where(row_is_nonlocal)[0]
    nlc = np.array(nlc, dtype=int)

    with suppress(ZeroDivisionError):
        print(
            f"{M.shape[1]-len(nlc)} local pairs "
            f"({(M.shape[1]-len(nlc)) / M.shape[0]:.0%} of rows, "
            f"{(M.shape[1]-len(nlc)) / M.shape[1]:.0%} of columns)"
        )
    if first_phase_only:
        return nlr, nlc 
    
    # Remove entries in local rows from non-local columnw
    new_columns = []
    with SDuration("Phase II"):
        for j in nlc:
            new_column = []
            q = A[j]
            if len(q) == 0: pass
            p = 0
            i = q[0]
            active_summands = []
            while i != 1:
                # If Row i is local, remove by column addition. Otherwise, proceed w. next.
                insert_ipq(active_summands, p+1, q)
                if (v := pivots[-i]) != -1:
                    insert_ipq(active_summands, 1, A[pivots[-i]])
                else:
                    new_column.append(new_indices[-i])
                i, p, q = pivot_ipq(active_summands)
            new_columns.append(new_column)
    
    result = GM(
            HeapMatrix( new_columns, (len(nlr), -1) ),
            M.row_grades[row_is_nonlocal],
            M.col_grades[nlc]
        )
    return result, nlr, nlc

def minimize_by_rows(M: GM, first_phase_only=False, method=minimize_by_columns):
    # Apply `method` to transpose of `M`.
    # Non-local rows of `M.T` are non-local columns of `M` and vice versa.
    # Indices must be reversed b/c result.D() flips order of rows and columns.
    result, nlr, nlc = method(M.D(same_entry_type=False), first_phase_only)
    return result.D(same_entry_type=False), \
        M.shape[0] - 1 - np.flip(nlc), \
        M.shape[1] - 1 - np.flip(nlr)

from typing import Iterable
def chunk(matrices: Iterable, start=0, stop=inf, reverse=False):
    ''' Iterate through the complex represented by `matrices`. 
        Apply chunk reduction (minimization) to `matrices[start]`,...,`matrices[stop-1]`.
        The rest is output without minimization.
        If `forward == True`, it is assumed that the matrices come in forward direction;
        i.e., that `matrices[i+1] @ matrices[i]` makes sense (and is zero). '''
    nlr = slice(None)
    nlc = slice(None)
    D1  = None
    # If the matrices are provided in forward order, the columns of the matrix D
    # currently being minimized correspond to the rows of the last matrix D1,
    # and vice versa if the matrices are provided in reverse order.
    if reverse:
        def t(r, _): return (slice(None), r)
        def s(_, c): return (c, slice(None))
    else:
        def s(r, _): return (slice(None), r)
        def t(_, c): return (c, slice(None))
    matrices = iter(matrices)
    # If start > 0, yield the first start-2 matrices unchanged. Put the (start-1)-st matrix
    # on hold as D1.
    overall_compression = [0, 0]
    def print_compression(D1, sh1):
        l = sum(len(d) for d in D1.entries.data)
        overall_compression[1] += l
        print(f"Shape of D{start} before: {(sh[0], sh[1])}")
        print(f"            after:  {D1.shape}")
        print(f"Compression rate:   {l / sh1[2]:.2%}")
        print(f"Rel matrix size:    ({D1.shape[0] / sh1[0]:.0%}, {D1.shape[1] / sh1[1]:.0%})")
    try:
        if start > 0:
            yield from it.islice(matrices, start-1)
            D1 = next(matrices)
        while start < stop:
            D = next(matrices)
            sh = (*D.shape, sum(len(d) for d in D.entries.data))
            overall_compression[0] += sh[2]
            with Duration("Chunk"):
                D, nlr, nlc = minimize_by_columns(D[s(nlr, nlc)])
                if D1 is not None:
                    D1 = D1[t(nlr, nlc)]
                    # print_compression(D1, sh1)
            if D1 is not None:
                yield D1
            D1 = D
            sh1 = sh
            start += 1
        # print_compression(D1, sh1)
        yield D1
        D1 = None
        # Yield the remaining matrices, if stop was not None.
        D = next(matrices)
        # print(f"Shape of D{start}: {D.shape}")
        D = D[s(nlr, nlc)]
        # print(f"Shape of chunked D{start}: {D.shape}")
        yield D
        yield from matrices
    except StopIteration:
        if D1 is not None:
            print_compression(D1, sh1)
            yield D1
    print(f"Overall compression rate: {overall_compression[1] / overall_compression[0]:%}")
