#!/usr/bin/env python3
import builtins
import multiprocessing
from more_itertools import peekable
from numpy import inf
from relative_cohomology import rel_cohomology_main, rel_cohomology_main_new
from sparse import HeapMatrix
import numpy as np, itertools as it, math, functools as ft
from copy import deepcopy
from utilities import SDuration, Silence, is_colex_sorted, lex_keys, lex_sorted, print, Duration, sdiv
from complexes import Bigrades
from matrices import matrices_from_file, GM
from factorisation import factor_GM, factor_ptr_heap, factor_smartly_left, factor_smartly_right
from minimization_routines import minimize_by_columns, minimize_by_rows, minimize_ptr, chunk
from kernel_routines import CohomologyKernelData, cohomology_kernel_heap_lazy, cohomology_kernel_heap_lazy_D̰, matrix_nullity, matrix_rank, is_injective
from utilities import neg_inf
np.set_printoptions(threshold=10000, linewidth=1000)

def free_resolution_grades(grades: Bigrades) -> tuple[Bigrades,Bigrades,Bigrades]:
        """ Returns a triple `(r0, r1, r2)` of grades for bases of a free
            resolution of the cofree module with basis grades `grades`."""
        return (
            np.array([(-inf,-inf) for _,_ in grades]),
            np.array([(-inf, y+1) for _,y in grades] + [(x+1, -inf) for x,_ in grades]),
            np.array([(x+1, y+1) for x,y in grades])
        )

def resolution_matrix_N(D1: GM, D2:GM, S1: np.ndarray, S2: np.ndarray):
    _,    dim0 = D1.shape
    dim2, dim1 = D2.shape
    return GM(
            HeapMatrix.block([
                [D1.entries,                        HeapMatrix.eye(dim1)[:,S1],        HeapMatrix.eye(dim1),              HeapMatrix.zeros((dim1, dim2)) ],
                [HeapMatrix.zeros((dim2, dim0)),   D2[S2,:][:,S1].entries,             HeapMatrix.zeros((dim2, dim1)),    HeapMatrix.eye(dim2)[S2,:]     ],
                [HeapMatrix.zeros((dim2, dim0)),   HeapMatrix.zeros((dim2, dim1)),    D2.entries,                         HeapMatrix.eye(dim2)           ]
            ]),
            np.vstack((D2.col_grades-(inf,inf), D2.row_grades[S2,:]-(-1,inf), D2.row_grades-(inf,-1))),
            np.vstack((D1.col_grades-(inf,inf), D2.col_grades[S1,:]-(-1,inf), D2.col_grades-(inf,-1), D2.row_grades-(-1,-1)))
        )

def resolution_matrix_M(D1: GM, D2:GM, S1: np.ndarray, S2: np.ndarray):
    _,    dim0 = D1.shape
    dim2, dim1 = D2.shape
    return GM(
            HeapMatrix.block([[D2.entries, HeapMatrix.eye(dim2)[:,S2], HeapMatrix.eye(dim2)]]),
            D2.row_grades-(inf,inf),
            np.vstack((D2.col_grades-(inf,inf), D2.row_grades[S2,:]-(-1,inf), D2.row_grades-(inf,-1)))
        )

def main_cohomology(matrices, smart=True):
    def kernel_domain_grades(D2, S2):
        return np.vstack((D2.col_grades-(inf,inf), D2.row_grades[S2,:]-(-1,inf), D2.row_grades-(inf,-1)))
    
    for d, D2 in enumerate(matrices):
        if d == 0:
            D1 = GM.zeros(D2.col_grades, np.empty((0,2), int))
            S1 = lex_keys(D2.col_grades)
            dim1 = D2.shape[1]
            K0 = CohomologyKernelData(
                V0              = HeapMatrix.block([[HeapMatrix.eye(dim1)], [HeapMatrix.eye(dim1)[:,S1]]]), # V0 = ['E' \\ E']
                V0_pivot_cols   = np.concatenate((np.full(dim1, -1), S1)),
                D̃               = HeapMatrix.zeros((dim1,0)),
                W̃               = HeapMatrix.zeros((0,0)),
                D_kernel_indices_inv = np.empty(0, int),
                V0_partner_cols = S1,
                V0_Er_cols = np.arange(dim1),
                V0_col_grades = D2.col_grades[S1,:]-(-1,-1)
            )
        with Duration(f"{d}-cohomology"):
            S2 = lex_keys(D2.row_grades)
            dim1, dim0 = D1.shape
            
            with Duration("Kernel") as duration_kernel:
                Z = deepcopy(D2)
                if smart: K1 = cohomology_kernel_heap_lazy_D̰(Z, S2, K0.D̃, S1=S1, clear_D̰=K0.D̰)
                else:     K1 = cohomology_kernel_heap_lazy(Z, S2, K0.D̃)
                K1_GM = GM(K1.V0, kernel_domain_grades(D2, S2), K1.V0_col_grades)
            
            with SDuration("Obtain L"):
                L_f = GM(
                        factor_smartly_left(D2.entries, K0), 
                        np.vstack((
                            D1.col_grades-(inf,inf), 
                            D2.col_grades[S1,:]-(-1,inf), 
                            D2.col_grades-(inf,-1), 
                            D2.row_grades-(-1,-1)
                        )),
                        np.array(K0.V0_col_grades)
                    )
                colex_keys = np.lexsort(L_f.row_grades.T)
                L_f = L_f[colex_keys,:]
            
            with SDuration("Truncate and minimize L"):
                # Columns of N_g that will factor through a single partner cancellation column, resulting in local column of N_f.
                cols_to_delete = np.array([
                        j for j, k in enumerate(colex_keys) 
                        if k >= (dim0 + 2*dim1) and K1.V0_partner_cols[k - (dim0 + 2*dim1)] != -1
                    ], int)
                # Corresponding local rows of N_f.
                rows_to_delete = K1.V0_partner_cols[colex_keys[cols_to_delete] - (dim0 + 2*dim1)]
                # Rows and columns of N_f that are not in rows/cols_to_delete
                kept_rows = np.isin(np.arange(K1.V0.shape[1]),  rows_to_delete, assume_unique=True, invert=True).nonzero()[0]
                kept_cols = np.isin(np.arange(len(colex_keys)), cols_to_delete, assume_unique=True, invert=True).nonzero()[0]
              
                L_t = L_f[kept_cols, :]
                L_m, nlr, _ = minimize_ptr(L_t)
            
            if smart:
                with SDuration("Smart factorisation of N"):
                    N_f = factor_smartly_right(S1, S2, K0, K1, colex_keys[kept_cols[nlr]])
            else:
                with SDuration("Naive factorisation of N"):
                    N_t = resolution_matrix_N(D1, D2, S1, S2)[:, colex_keys[kept_cols[nlr]]]
                    N_f = factor_ptr_heap(N_t.entries, K1.V0, lor=False, grades=N_t.col_grades)
            

            N_f = GM(
                N_f[kept_rows,:], 
                K1.V0_col_grades[kept_rows], 
                L_m.row_grades
            )
            with SDuration("Minimize N̂"):  
                N_m, _, nlc = minimize_by_columns(N_f)
                L_m = L_m[nlc,:]
            

            D1 = D2
            S1 = S2
            K0 = K1
            K0_GM = K1_GM
        yield (L_m, N_m)
    
from implementation import kernel_2d_sparse
def main_homology(matrices):
    """ Computes homology in ascending dimensions. """
    # - compute Zᵢ₊₁ and a minimal presentation ∂̃ᵢ₊₁ of Bᵢ.
    # - factor ∂̃ᵢ₊₁ through Zᵢ
    # - minimize the resolution.
    matrices = peekable(matrices)
    # Reduced 0-cycles
    z = kernel_2d_sparse(
        # Augmenting (boundary) map from the 0- to the -1-chains
        GM.ones(
            np.array([[neg_inf, neg_inf]]), 
            matrices.peek().row_grades
        )
    )
    for d, D2 in enumerate(matrices, 0):
        with Duration(f"{d}-homology"):
            with SDuration("MGS/Kernel"):
                z1, mgs, ker_mgs = kernel_2d_sparse(deepcopy(D2), mgs=True, ker_mgs=True)
            with SDuration("Consolidate"):
                z1.entries.consolidate()
            with SDuration("Minimize left"):
                f2_min, nlr, _ = minimize_by_columns(ker_mgs)
            with SDuration("Factor"):
                f1 = factor_GM(mgs[:,nlr], z)
            with SDuration("Minimize right"):
                f1_min, _, nlc = minimize_by_columns(f1)
                f2_min = f2_min[nlc,:]
            z = z1
        yield (f1_min, f2_min)

def homology_reverse(matrices, start=0):
    """ Computes homology in descending degrees. """
    matrices = iter(matrices)
    mgs = None
    D1  = next(matrices)
    D   = next(matrices)
    # In top dimension n, need a mgs of n-boundaries.
    with Duration(f"{start}-homology"):
        with SDuration("MGS/Kernel"):
            _, mgs, rel = kernel_2d_sparse(D1, mgs=True, ker_mgs=True)
        with SDuration("MGS/Kernel"):
            z1, mgs1, rel1 = kernel_2d_sparse(D, mgs=True, ker_mgs=True)
            assert not (np.unique(z1.entries.pivot_rows(), return_counts=True)[1] > 1).any()
        with SDuration("Minimize left"):
            f2_min, nlr, _ = minimize_ptr(rel)
        with SDuration("Factor"):
            f1 = factor_GM(mgs[:,nlr], z1)
        with SDuration("Minimize right"):
            f1_min, _, nlc = minimize_by_rows(f1)
            f2_min = f2_min[nlc,:]
    del D1
    mgs, rel = mgs1, rel1
    # In the following dimensions i, we reuse the mgs of i-boundaries computed for Hᵢ₊₁.
    for d, D in zip(it.count(start-1, -1), matrices):
        with Duration(f"{d}-homology"):
            with SDuration("MGS/Kernel"):
                z1, mgs1, rel1 = kernel_2d_sparse(D, mgs=True, ker_mgs=True)
            with SDuration("Minimize left"):
                f2_min, nlr, _ = minimize_ptr(rel)
            with SDuration("Factor"):
                f1 = factor_GM(mgs[:,nlr], z1)
            with SDuration("Minimize right"):
                f1_min, nlc = minimize_by_rows(f1)
                f2_min = f2_min[nlc,:]
            mgs, rel = mgs1, rel1
        yield (f1_min, f2_min)
    # In dimension 0, we needn't compute any kernel.
    with Duration(f"{0}-homology"):
        with SDuration("Minimize left"):
            f2_min, nlr, _ = minimize_ptr(rel)
        with SDuration("Minimize right"):
            f1 = mgs[:,nlr]
            f1_min, _, nlc = minimize_by_rows(f1)
            f2_min = f2_min[nlc,:]

@Silence()
def is_resolution(F2: GM, F1: GM):
    """ Checks if the maps `F2`, `F1` of maps of free moduels form a resolution.
        This is the case if ker(`F1`) = im(`F2`), and `F2` is injective."""
    assert F1.is_well_defined() and F1.is_minimal()
    assert F2.is_well_defined() and F2.is_minimal()
    assert not (F1 @ F2).entries.any()
    assert is_injective(deepcopy(F2.entries))
    assert F2.shape[1] == matrix_nullity(deepcopy(F1.entries))
    return True

def homology2cohomology(resolution):
    """ builds a cohomology resolution from the given cohomology `resolution`, according to the curious flipping lemma. """
    return (resolution[1].D(True), resolution[0].D(True))

def sphere_example_matrices(transpose=False, stop=None):
    def helper():
        m1 = GM(
            HeapMatrix(np.array([[1,1], [1,1], [1,1]])),
            np.array([[0, -2], [-1,-1], [-2, 0]]),
            np.array([[0,0], [0,0]])
        )
        m2 = GM(
            HeapMatrix(np.array([[1,0,1], [1,1,0], [0,1,1]])),
            np.array([[-2,-2], [-1,-2], [-2,-1]]),
            np.array([[0, -2], [-1,-1], [-2, 0]])
        )
        m3 = GM.zeros(
            np.empty((0, 2), int),
            np.array([[-2,-2], [-1,-2], [-2,-1]]), 
        )
        assert is_colex_sorted(m1.col_grades)
        assert is_colex_sorted(m1.row_grades)
        assert is_colex_sorted(m2.row_grades)
        assert m1.is_well_defined()
        assert m2.is_well_defined()
        assert not (m2 @ m1).entries.any()
        assert not (m3 @ m2).entries.any()
        if transpose:
            yield m1.D(True)
            yield m2.D(True)
            yield m3.D(True)
        else:
            yield m1
            yield m2
            yield m3
    yield from it.islice(helper(), stop)


def check_same_output(filename):
    """ Checks if all (co)homology computation methods give the same resolution grades."""
    #m = ft.partial(matrices_from_file, filename)
    #m = sphere_example_matrices
    n = 2
    for d, results in enumerate(zip(
        # map(homology2cohomology, main_homology(m(True, stop=n+1))),
        it.islice(rel_cohomology_main_new(m(stop=n+2)), 1, None),
        it.islice(rel_cohomology_main(m(stop=n+1)), 1, None),
    )):
        builtins.print("In dimension", d, ":")
        # Each result in `results` consists of two graded matrices, that together form a minimal graded free resolution.
        # These resolutions need not be equal; however, they must be homotopy equivalent. In particular, the degrees
        # that occur in both must be equal, up to reordering. Sort the grades lexicographically and check if all results
        # in `results` have the same:
        for r1, r2 in results:
            print(r1.shape, r2.shape)
            assert is_resolution(r2, r1)
            # assert not np.any(r1.row_grades == neg_inf)
            # assert not np.any(r1.col_grades == neg_inf)
            # assert not np.any(r2.col_grades == neg_inf)
            #assert not (r1 @ r2).entries.any()
        g = [(lex_sorted(r[0].row_grades), lex_sorted(r[0].col_grades), lex_sorted(r[1].col_grades)) for r in results]
        a = np.array([[[np.array_equal(g1, g2) for g1, g2 in zip(r1, r2)] for r2 in g] for r1 in g])
        b = np.array([[[np.array_equal(g1[np.all(g1 != neg_inf, 1)], g2[np.all(g2 != neg_inf, 1)]) for g1, g2 in zip(r1, r2)] for r2 in g] for r1 in g])
        assert np.all(a)
    

import sys
if __name__ == "__main__":
    filenames = sys.argv[1:]
    n = 2
    multiprocessing.set_start_method('fork')
    for i, filename in enumerate(filenames):
        # Maximal (co)homology dimension; need matrices up to D_{n+1} (or even D_{n+2}).
        m = ft.partial(matrices_from_file, filename)
        # m = sphere_example_matrices
        
        # check_same_output(filename)
        
        for message, task in [
         #   ("Homology, increasing dimensions, no chunk", lambda: main_homology(m(True, stop=n+1))),
        #     # ("Homology, increasing dimensions, chunk up to Dₙ", lambda: main_homology(chunk(m(True, stop=n+1), stop=n, reverse=True))),
        #     # ("Homology, increasing dimensions, chunk up to Dₙ₊₁", lambda: main_homology(chunk(m(True, stop=n+2), reverse=True))),
        #    ("Abs. cohomology, increasing dimensions, no chunk", lambda: main_cohomology(m(stop=n+1))),
        #     # ("Abs. cohomology, increasing dimensions, chunk up to Dₙ", lambda: main_cohomology(chunk(m(stop=n+1), stop=n))),
        #     # ("Abs. cohomology, increasing dimensions, chunk up to Dₙ₊₁", lambda: main_cohomology(chunk(m(stop=n+2), stop=n))),
            # ("Rel. cohomology, increasing dimensions, no chunk", lambda: rel_cohomology_main(m(stop=n+2))),
        #     # ("Rel. cohomology, increasing dimensions, chunk up to Dₙ", lambda: list(rel_cohomology_main(m(stop=n+1)))),
        #     # ("Rel. cohomology, increasing dimensions, chunk up to Dₙ₊₁", lambda: list(rel_cohomology_main(m(stop=n+2)))),
            ("Rel. cohomology (new), increasing dimensions, no chunk", lambda: rel_cohomology_main_new(m(stop=n+1))),
        #     # ("Rel. cohomology (new), increasing dimensions, chunk up to Dₙ", lambda: list(rel_cohomology_main_new(m(stop=n+1)))),
        #     # ("Rel. cohomology (new), increasing dimensions, chunk up to Dₙ₊₁", lambda: list(rel_cohomology_main_new(m(stop=n+2)))),
        ]:
            print(message)
            for resolution in task():
                print("size of the resolution:", resolution[0].shape, resolution[1].shape)
        if i < len(filenames)-1:
            print('\x1b]1337;SetMark\x07')

