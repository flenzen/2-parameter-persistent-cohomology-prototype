from contextlib import contextmanager
from copy import deepcopy
import multiprocessing
import numpy as np
import numpy.random as random
import pytest
from kernel_routines_test import resolution_matrices_for_testing, kernel_data, boundary_matrices
from sparse import HeapMatrix, ListMatrix
from factorisation import factor, factor_p, factor_ptr_heap, factor_ptr_heap_p, factor_smartly_right, factor_smartly_left

@pytest.fixture(params=[(90, 100, 50, 0)])
def factorisation_problem(request):
    # Build factorisation problem (T, N @ T) with parameters
    # 1) number of rows of T
    # 2) number of rows of N
    # 3) number of columns of T/rows of N
    # 4) seed
    s, t, u, v = request.param
    random.seed(v)
    T = np.zeros((s, s), int)
    T[np.triu_indices(s)] = random.randint(0, 2, s*(s+1)//2)
    T[np.diag_indices(s)] = 1
    T = T[:, random.choice(np.arange(s), u, False)]
    N = random.randint(0, 2, (u, t))
    M = T @ N % 2
    return (HeapMatrix(T), HeapMatrix(M))

@pytest.mark.parametrize("factorisation_function", [factor, factor_p, factor_ptr_heap, factor_ptr_heap_p])
@pytest.mark.parametrize("matrix_type", [HeapMatrix, ListMatrix])
def test_factorisation_function(factorisation_function, matrix_type, factorisation_problem):
    with set_start_method("fork"):
        T, M = factorisation_problem
        T, M = matrix_type(T), matrix_type(M)
        F = factorisation_function(deepcopy(M), deepcopy(T))
        assert np.array_equal(T @ F, M)

        pivots = np.full(T.shape[0], -1)
        pivots[T.pivot_rows()] = np.arange(T.shape[1])
        F = factorisation_function(deepcopy(M), deepcopy(T), pivots)
        assert np.array_equal(T @ F, M)

@contextmanager
def set_start_method(method):
    start_method = multiprocessing.get_start_method()
    try:
        multiprocessing.set_start_method(method, force=True)
        yield
    finally:
        multiprocessing.set_start_method(start_method, force=True)

def test_cohomology_factorisation(boundary_matrices, kernel_data):
    D, S, S_inv, G = boundary_matrices
    dim1, dim0 = D[1].shape
    dim2, _    = D[2].shape
    R = resolution_matrices_for_testing(boundary_matrices)
    K0, K1 = kernel_data
    factorisation_smart = factor_smartly_right(S[1], S[2], K0, K1, range(dim0+2*dim1+dim2))
    factorisation_dumb  = factor(deepcopy(R.N), K1.V0)
    assert np.array_equal(K1.V0 @ factorisation_smart, R.N)
    assert np.array_equal(K1.V0 @ factorisation_dumb,  R.N)

def test_cohomology_factorisation_left(boundary_matrices, kernel_data):
    D, S, S_inv, G = boundary_matrices
    dim1, dim0 = D[1].shape
    dim2, _    = D[2].shape
    R = resolution_matrices_for_testing(boundary_matrices)
    K0, K1 = kernel_data
    
    # This is the assumption underlying `factor_smartly_left`.
    assert np.array_equal(
        D[2].entries[:,S[1]] @ K0.V0[dim0:dim0+dim1,:], 
        D[2].entries @ K0.V0[dim0+dim1:,:]
    )
    
    # The factorisation of L should work out properly:
    factorisation_smart = factor_smartly_left(D[2].entries, K0)
    factorisaiton_dumb  = factor(R.L @ K0.V0, R.U)
    assert np.array_equal(R.L @ K0.V0, R.U @ factorisation_smart)
    assert np.array_equal(R.L @ K0.V0, R.U @ factorisaiton_dumb)
    assert np.array_equal(factorisation_smart, factorisaiton_dumb)
    
