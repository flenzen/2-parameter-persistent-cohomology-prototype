from dataclasses import dataclass
from sparse import HeapMatrix, ListMatrix, LocMatrix
from sparse import insert_ipq, pivot_ipq, eval_ipq
from utilities import Duration, ProgressInfoIterator, ProgressIterator, SDuration, has_duplicates, integer_retraction, lex_keys, inverse_permutation, print
from matrices import GM
import numpy as np, copy, heapq, functools as ft, itertools as it
from dataclasses import dataclass
from copy import deepcopy

def matrix_rank(matrix):
    return reduce_ltr(matrix)[0].shape[1]

def matrix_nullity(matrix):
    return matrix.shape[1] - matrix_rank(matrix)

def matrix_conullity(matrix):
    return matrix.shape[0] - matrix_rank(matrix)

def is_injective(matrix):
    return matrix_nullity(matrix) == 0

@dataclass
class CohomologyKernelData:
    V0: HeapMatrix = None                       # Entire kernel matrix
    V0_pivot_cols: np.ndarray = None            # Pivot row -> column assignment of V0.
    D̃: HeapMatrix = None                        # Nonzero columns of reduced DV
    Ṽ: HeapMatrix = None                        # Columns of V that yield nonzero columns in DV.
    W̃: HeapMatrix = None                        # Left inverse of Ṽ.
    D̃_pivot_cols = None # TODO remove
    D̰: HeapMatrix = None
    V̰: HeapMatrix = None
    W̰: HeapMatrix = None 
    D_kernel_indices_inv: np.ndarray = None     # assignment column index i in D -> column index in V0 
                                                # if D[:,i] is zeroed out (-1 otherwise)
    V0_partner_cols: np.ndarray = None          # assignment column index i of E -> column index in V0
                                                # if eliminated by its partner (and -1 otherwise)
    V0_Er_cols: np.ndarray = None               # column indices where the reduced E' occurs in V0
    V0_col_grades: np.ndarray = None
    
    R: HeapMatrix = None                        # matrix s.th. `V0_D @ R` is completely reduced
    D̰_R: HeapMatrix = None                      # matrix s.th. `V0_D @ D̰_R` is completely reduced
    D̰_kernel_indices_inv = None                 # D̰_kernel_indices_inv[i] is the index of V[:,i] in V0_'D.

@Duration("Reduce")
def reduce_ltr(M: LocMatrix, C: LocMatrix = None, compute_W̊ = False):
    """ Arguments: 
        - M: matrix to reduce (changed)
        - C: matrix used for clearing
        Returns:
        - the nonzero columns of the reduced matrix M̃
        - the reduction matrix V
        - matrix W such that M = M̃W
        - indices V0_indices such that V0 = V[V0_indices]
        - its inverse
        - indices M̃_indices such that M̃ = MV[M̃_indices]
        - its inverse
        - the pivot row -> column assignment
    """
    n_rows, n_cols = M.shape
    V      = HeapMatrix.eye(n_cols)         # reduction matrix
    pivots  = np.full(n_rows, -1)
    V0_indices = []                         # indices of columns reduced to zero
    M̃_indices  = []                         # indices of columns not reduced to zero
    M_to_V0 = np.full(n_cols, -1)           # index of a zeroed-out column in V0
    M_to_M̃  = np.full(n_cols, -1)           # index of a not zeroed-out column in M̃
    W̃ = [[] for _ in range(n_cols)]         # inverse of V
    if C is not None:
        assert C.shape[0] == n_cols and not has_duplicates(C.pivot_rows())
        with SDuration("Consolidate clearing matrix"):
            C.consolidate()
        with SDuration("Clear"):
            cleared = []
            for j in range(C.shape[1]):
                i = C.pivot(j)
                M.erase_column(i)
                V[:,i] = C[:,j]
                cleared.append((i,j))
    unchanged_columns = 0
    with SDuration("Reduce"):
        for j in range(n_cols):
            first_round = True
            while (i := M.pivot(j)) != -1:
                if (v := pivots[i]) != -1:
                    M.column_operation(v, j)
                    V.column_operation(v, j)
                    # DV[:,v] = D̃[:,M_to_M̃[v]] has already attained its final state.  
                    # Therefore, we needn't compute W by row operations, but can build its columns:
                    W̃[j].append(-M_to_M̃[v])
                else:
                    if first_round:
                        unchanged_columns += 1
                    M_to_M̃[j] = len(M̃_indices)
                    W̃[j].append(-len(M̃_indices))
                    M̃_indices.append(j)
                    pivots[i] = j
                    break
                first_round = False
            else: 
                M_to_V0[j] = len(V0_indices)
                V0_indices.append(j)
    # print("Colums already reduced:", unchanged_columns)
    # print(f"|D| (unconsolidated):\t{sum(len(c) for c in M.data)} entries.")
    with SDuration("Consolidate reduced matrix"):
        M.consolidate()
    # print(f"|D| (consolidated):\t{sum(len(c) for c in M.data)} entries.")
    # print(f"|V| (unconsolidated):\t{sum(len(c) for c in V.data)} entries.")
    V.consolidate()
    # print(f"|V| (consolidated):\t{sum(len(c) for c in V.data)} entries.")
    W_entries = W̃
    for c in W̃:
        c.sort()
    W̃ = ListMatrix(W_entries, (n_cols-len(V0_indices), n_cols))
    V_ls = ListMatrix(V)
    R = HeapMatrix.eye(len(V0_indices))
    M̃ = M[:, M̃_indices]

    if C is not None:
        with SDuration("Compute W̃ for cleared columns"):
            # The columns W[:,i] for which D[:,i] has not been subject to clearing 
            # already have their correct value.  If we determine the remaining columns W[:,i]
            # in ascending order, D @ C[:,j] = 0 implies D[:,i] = D(C[:,j] - e_i).
            # All nonzero entries of (C[:,j] - e_i) are in rows i' < i; 
            # for these, these W[:,i'] already has its correct value, so D(C[:,j] - e_i) = D̃W(C[:,j] - e_i).
            # Since W[:,i] = 0 still, D̃W(C[:,j] - e_i) = D̃WC[:,j].
            cleared.sort()
            for i, j in cleared:
                W̃[:,i]  = W̃  @ C[:,j]
        with SDuration("Completely reduce V"):
            # Compute R such that V0·R is completely reduced.
            # Traverse columns j of V0 in ascending pivots order.  This ensurses that we needn't
            # re-evaluate the product V0·R to get the next entry k.
            for j in M_to_V0[M_to_V0 != -1]:
                for k in V_ls.data[V0_indices[j]][1:]:
                    if (v0 := M_to_V0[-k]) != -1:
                        R.column_operation(v0, j)
    if compute_W̊:
        # retraction of the kernel inclusion
        W0 = HeapMatrix([[] if i == -1 else [-i] for i in M_to_V0], (len(V0_indices), M.shape[1]))
        if C is not None:
            with SDuration("Compute W̊ for cleared columns"):
                for i, j in cleared:
                    W0[:,i] = W0 @ C[:,j]
    else:
        W0 = None
    return (M̃, V_ls, W̃, V0_indices, M_to_V0, M̃_indices, M_to_M̃, pivots, R, W0)

EP_reduction_heap_lazy_D̰ = None
@Duration("Cohomology kernel")
def cohomology_kernel(D: GM, S: np.ndarray, clearing: HeapMatrix = None, EP_reduction = EP_reduction_heap_lazy_D̰, **kwargs):
    """ D: coboundary matrix Cⁱ → Cⁱ⁺¹ for computing Hⁱ.
           Column and row grades of D must be in colex order (not checked).
        S: permutation colex → lex for rows of D
        G: grades for rows of D
        EP_block_reduction: callable with arguments
            pivots
            D_ls
    """
    assert D.shape[0] == len(S), "Input shapes don't match."
    G = D.row_grades + 1
    V_row_grades = np.vstack((D.col_grades-(np.inf,np.inf), G[S,:]-(0,np.inf), G-(np.inf,0)))
    dim2, dim1 = D.shape
    dim = dim1 + 2*dim2
    D_columns  = range(0, dim1)
    EP_columns = range(dim1, dim1+dim2)
    E_columns  = range(dim1+dim2, dim1+2*dim2)
    EP_grades  = G[S, 0]
    E_grades   = G[:, 1]
    
    D = D.entries

    K = CohomologyKernelData()
    with SDuration("Copy D"):
        D_ = copy.deepcopy(D)
    # region Reduction of the D-block.
    K.D̃, V_D, K.W̃, V0_pivot_rows, M_to_V0, D̃_to_D, D_to_D̃, K.D̃_pivot_cols, K.R, K.W̊ = reduce_ltr(D, deepcopy(clearing), compute_W̊=True)
    
    K.D_kernel_indices_inv = M_to_V0            # TODO remove?
    K.V0_D = deepcopy(V_D[:, V0_pivot_rows])    # TODO remove?

    K.D̃.consolidate()
    D_ls = [sorted(column) for column in D.data]
    K.Ṽ = V_D[:, D̃_to_D]
    K.V0 = ListMatrix(V_D[:, V0_pivot_rows]).data
    K.V0_col_grades = [(-np.inf, -np.inf) for _ in V0_pivot_rows]
    K.V0_pivot_cols = np.concatenate((M_to_V0, np.full(2*dim2, -1)))
    
    # TODO remove?
    K.D_kernel_indices_inv = M_to_V0
    K.V0_D = deepcopy(V_D[:, V0_pivot_rows])
    
    V0D = V_D[:, V0_pivot_rows]
    V0D_to_D = deepcopy(V0_pivot_rows)
    # endregion
    
    # region Reduction of E
    reduce_E_block(K, E_columns, E_grades, V_D, D_ls, V0_pivot_rows)
    # Get the number of E-columns that are not reduced yet, and thus reduced at a finite degree.
    number_finite_kernel_grades = np.count_nonzero(K.D̃_pivot_cols == -1)
    # endregion
    
    # region Reduction of the E'-block.
    EP_reduction(K, D_columns, EP_columns, EP_grades, E_grades, E_columns, S, V_D, D_ls, V0_pivot_rows, D_, 
        D̃_to_D = D̃_to_D, 
        D_to_D̃ = D_to_D̃, 
        V0D = V0D, 
        D_to_V0D = M_to_V0,
        V0D_to_D = V0D_to_D,
        C = deepcopy(clearing),
        **kwargs)
    no_partner_cancellation = np.count_nonzero(K.V0_partner_cols != -1)
    # endregion      
    
    if number_finite_kernel_grades > 0:
        print(f"E-columns cancelled at finite degree: {number_finite_kernel_grades/dim2:4.0%}, "
              f"of which cancelled by partner:",
              "all" if no_partner_cancellation==number_finite_kernel_grades 
              else f"{no_partner_cancellation/number_finite_kernel_grades:4.0%}")
    for c in K.V0:
        heapq.heapify(c)
    K.V0 = HeapMatrix(K.V0, (dim, -1))
    K.V0_col_grades = np.array(K.V0_col_grades)
    return K

@SDuration("Reduce block E")
def reduce_E_block(K: CohomologyKernelData, E_columns, E_grades, V_D, D_ls, V0_pivot_rows):
    # If a column j occurs as a pivot of D̃[:,k], it will be reduced to zero.  The respective
    # kernel column is (Ṽ[:,k] \\ 0 \\ D̃[:,k])
    assert isinstance(V_D, ListMatrix)
    for j in E_columns:
        i = j - E_columns.start
        if (v := K.D̃_pivot_cols[i]) != -1:
            K.V0_pivot_cols[j] = len(V0_pivot_rows)
            K.V0.append([k - E_columns.start  for k in D_ls[v]] + V_D.data[v])
            V0_pivot_rows.append(j)
            K.V0_col_grades.append((-np.inf, E_grades[i]))

@Duration("Reduce block E' the new way")
def EP_reduction_heap_lazy_D̰(
        K: CohomologyKernelData,
        D_columns, EP_columns, EP_grades, E_grades, E_columns, S, V_D, D_ls, V0_pivot_rows, D_orig, use_D̰ = True, **kwargs
    ):
    """ Like `EP_reduction_heap_lazy`, but computes D̰ and uses it in V̊ for the columns E'
        that are reduced to zero.  The other columns of E' (that are not reduced to zero),
        and the columns of E, are reduced as in `EP_reduction_heap_lazy`. 
     """
    dim2                    = len(EP_columns)
    dim1                    = len(D_columns)
    S_inv                   = np.full(dim2, -1)                     # inverse of S (populated later)
    K.V0_Er_cols = []
    K.V0_partner_cols = np.full(dim2, -1)
    with SDuration("Consolidate V"):
        K.Ṽ.consolidate()
    with SDuration("Consolidate W"):
        K.W̃.consolidate()
    
    if use_D̰:
        # with Duration("Compute D̰ from D[:, D̃_columns]"):
        #     with SDuration("Reorder"): 
        #         Z = D_orig[S, kwargs['D̃_to_D']]
        #     with Duration("Reduce"):
        #         K.D̰, X, Y, *_ = reduce_ltr(Z)
        #     with SDuration("Reindex"):
        #         D̃_to_D = kwargs['D̃_to_D']
        #         D_to_D̃ = kwargs['D_to_D̃']
        #         K.V̰ = X.reindex_rows(D̃_to_D, dim1)
        #         U = Y @ kwargs['V0D'][D̃_to_D,:] @ K.R
        #         K.W̰ = HeapMatrix([
        #             Y.data[D_to_D̃[j]] if D_to_D̃[j] != -1 else U.data[kwargs['D_to_V0D'][j]]
        #             for j in range(dim1)
        #         ], (Y.shape[0], dim1) )
        #         K.D̰_kernel_indices_inv = K.D_kernel_indices_inv
        #         K.D̰_R = K.R

        # with Duration("Compute D̰ from D[:, D̃_columns][:, Π]"):
        #     with SDuration("Reorder"): 
        #         D̃_to_D = np.array(kwargs['D̃_to_D'])
        #         D_to_D̃ = kwargs['D_to_D̃']
        #         S1     = kwargs["S1"]
        #         T      = np.argsort(S_inv[D̃_to_D])
        #         T_inv  = inverse_permutation(T)
        #         Z      = D_orig[S, D̃_to_D[T]]
        #     with Duration("Reduce"):
        #         K.D̰, X, Y, *_ = reduce_ltr(Z)
        #     with SDuration("Reindex"):
        #         X = X[T_inv,:]
        #         Y = Y[:,T_inv]
        #         # From here, like above.
        #         K.V̰ = X.reindex_rows(D̃_to_D, dim1)
        #         U = Y @ kwargs['V0D'][D̃_to_D,:] @ K.R
        #         K.W̰ = HeapMatrix([
        #             Y.data[D_to_D̃[j]] if D_to_D̃[j] != -1 else U.data[kwargs['D_to_V0D'][j]]
        #             for j in range(dim1)
        #         ], (Y.shape[0], dim1) )
        #         K.D̰_kernel_indices_inv = K.D_kernel_indices_inv
        #         K.D̰_R = K.R
            
        # with Duration("Compute D̰ from D̃"):
        #     with SDuration("Reorder"): 
        #         Z = K.D̃[S,:]
        #     with Duration("Reduce"):
        #         K.D̰, X, Y, *_ = reduce_ltr(Z)
        #     with SDuration("Evaluate V, W"):
        #         K.V̰ = K.Ṽ @ X
        #         K.W̰ = Y @ K.W̃
        #         K.D̰_kernel_indices_inv = K.D_kernel_indices_inv
        #         K.D̰_R = K.R

        # with Duration("Compute D̰ from D̃[:, Π]"):
        #     with SDuration("Reorder"): 
        #         D̃_to_D = np.array(kwargs['D̃_to_D'])
        #         D_to_D̃ = kwargs['D_to_D̃']
        #         S1     = kwargs["S1"]
        #         T      = np.argsort(S_inv[D̃_to_D])
        #         T_inv  = inverse_permutation(T)
        #         Z = K.D̃[S, T]
        #     with Duration("Reduce"):
        #         K.D̰, X, Y, *_ = reduce_ltr(Z)
        #     with SDuration("Evaluate V, W"):
        #         K.V̰ = K.Ṽ @ X[T_inv, :]
        #         K.W̰ = Y[:,T_inv] @ K.W̃
        with Duration("Compute D̰ from D[:, Π] w. clearing"):
            with Duration("Reorder"): 
                S1 = kwargs['S1']
                S1_inv = inverse_permutation(S1)
                Z = D_orig[S, S1]
            with Duration("Reduce"): 
                D̰ = kwargs.get('clear_D̰', None)
                K.D̰, X, Y, _, _,D̰_columns, *_, D̰_R, _ = reduce_ltr(Z, D̰)
            with Duration("Reorder"): 
                D̰_ker_columns = np.setdiff1d(np.arange(dim1), D̰_columns)

                K.V̰ = X[S1_inv, D̰_columns]
                K.W̰ = Y[:,S1_inv]
                D̰ = K.D̰
                K.D̰_R = K.W̊ @ X[S1_inv, D̰_ker_columns] @ D̰_R

                K.D̰_columns = S1[D̰_columns]
                K.D̰_ker_columns = S1[D̰_ker_columns]
                K.D̰_kernel_indices_inv = integer_retraction(K.D̰_ker_columns, K.W̰.shape[1])
        
    
        with SDuration("Convert to ListMatrices"):
            D̰_ls = ListMatrix(K.D̰)
            V̰_ls = ListMatrix(K.V̰)
            D̰_pivots = np.full(dim2, -1)
            D̰_pivots[K.D̰.pivot_rows()] = range(K.D̰.shape[1])
    
    D_ls.extend([-S[EP_columns.index(j)]] for j in EP_columns)
    V_D_data = V_D.data + [[-j] for j in EP_columns]
    kernel_blocks = []
    active_summands = []
    pivots = copy.deepcopy(K.D̃_pivot_cols)

    number_eliminated=0 #TODO remove
    number_changed=0 #TODO remove
    with SDuration("Reduce E' and E"):
        for grade, block in it.groupby(EP_columns, lambda j: EP_grades[j - EP_columns.start]):
            # column indices j in E' eliminated
            eliminated = []
            # pairs (i, j) of column indices j in E' not eliminated, and their pivot i.
            eliminating = []
            for j in block:
                if use_D̰ and (v := D̰_pivots[j - EP_columns.start]) != -1:
                    # Use the resp. column of (V̰ \\ 'D̰ \\ 0)[:,v] as a kernel column.
                    V_D_data[j] = [x - dim1 for x in D̰_ls.data[v]] + V̰_ls.data[v]
                    eliminated.append(j)
                    number_eliminated+=1 #TODO remove
                else:
                    # Ordinary reduction.  Column will not be red. to zero.
                    q = D_ls[j]
                    p = 0
                    i = -S[EP_columns.index(j)]
                    if pivots[-i] != -1: #TODO remove
                        number_changed += 1
                    while i != 1:
                        if (k := pivots[-i]) != -1:                                          
                            # realize column addition
                            if S_inv[-i] != -1:
                                # Reduce by original state of another E'-column.
                                V_D_data[j].append(-S_inv[-i])
                            else:
                                # Reduce by column of D.
                                s = D_ls[k]
                                V_D_data[j].extend(V_D_data[k])
                                insert_ipq(active_summands, 1, s)
                            insert_ipq(active_summands, p+1, q)
                            i, p, q = pivot_ipq(active_summands)
                        else:
                            # column stays as is; write back
                            pivots[-i] = j
                            D_ls[j] = eval_ipq(active_summands, p, q)
                            # Column j induces elimination of E[:,i]; store for later assembly of new kernel column.
                            eliminating.append((-i, j))
                            break
                    else:
                        # A column j not occurring as a pivot of D̰ should not be reduced to zero.
                        assert not use_D̰
                        eliminated.append(j)
                    S_inv[S[EP_columns.index(j)]] = j
                    active_summands.clear()
            kernel_blocks.append((grade, eliminated, eliminating))
        # The following part could be run in parallel to the above loop and process the data further
        # while the above loop continues.
        for grade, eliminated, eliminating in ProgressInfoIterator(kernel_blocks, "E'-block (build kernel columns)"):
            # kernel elements at (finite, infinite)
            l0 = len(K.V0)
            l1 = l0 + len(eliminated)
            l2 = l1 + len(eliminating)
            K.V0_pivot_cols[eliminated] = np.arange(l0, l1)
            K.V0.extend([V_D_data[j] for j in eliminated])
            K.V0_col_grades.extend([(grade, -np.inf)] * len(eliminated))
            V0_pivot_rows.extend(eliminated)
            # kernel elements at (infinite, finite)
            eliminating.sort()
            for i, j in eliminating: # (i = pivot(M[:,j]))
                j0 = len(K.V0) # index of new kernel column
                if i == S[EP_columns.index(j)]:
                    K.V0_partner_cols[i] = j0
                K.V0_pivot_cols[E_columns[i]] = j0
                K.V0_Er_cols.append(j0)
                K.V0_col_grades.append((grade, E_grades[i]))
                V0_pivot_rows.append(E_columns[i])
                K.V0.append([k - E_columns.start for k in D_ls[j]] + V_D_data[j])
    print("Eliminated E'-columns:", number_eliminated, "Changed (but not eliminated) columns of E':", number_changed, "number of E'-columns:", len(EP_columns))

EP_reduction_heap_lazy = ft.partial(EP_reduction_heap_lazy_D̰, use_D̰ = False)
cohomology_kernel_heap_lazy_D̰ = ft.partial(cohomology_kernel, EP_reduction = EP_reduction_heap_lazy_D̰)
cohomology_kernel_heap_lazy = ft.partial(cohomology_kernel, EP_reduction = EP_reduction_heap_lazy)

def kernel_sort(columns, grades, pivots, Er_columns, partner_cancellation):
    """ Sorts all relevant kernel data in lex order.
        Kernel columns on D- and E-block are already in lex order, E'-block
        and E'-generated E-columns are not. We may process E' in blocks and
        order the kernel columns after each block, or order all columns when
        processing of E' is done. The latter is done by this method."""
    keys  = lex_keys(grades)
    ikeys = inverse_permutation(keys)
    columns[:] = [columns[j] for j in keys]
    grades[:] = [grades[j] for j in keys]
    pivots[pivots != -1] = ikeys[pivots[pivots != -1]]
    Er_columns[:] = ikeys[Er_columns]
    partner_cancellation[partner_cancellation != -1] = ikeys[partner_cancellation[partner_cancellation != -1]]

def reduce_ltr_initial_values(M: LocMatrix, V = None, W = None):
    """ Re-implementation of the ltr-reduction. If provided, uses starting values for `V` and  `W`.
        Changes the original `M`."""
    pivots = np.full(M.shape[0], -1)
    if V is None:
        V = HeapMatrix.eye(M.shape[1])
    else:
        assert V.shape[1] == M.shape[1], "`V` and `M` msut have the same number of columns."
    if W is None:
        W = HeapMatrix.eye(M.shape[1]).T()
    else:
        assert W.shape[0] == M.shape[1], "`W` must have as many rows as `M` has columns."
    for j in range(M.shape[1]):
        while (i := M.pivot(j)) != -1:
            if pivots[i] != -1:
                M.column_operation(pivots[i], j)
                V.column_operation(pivots[i], j)
                W.row_operation(j, pivots[i])
            else:
                pivots[i] = j
                break
        else: 
            assert False, "Use this only for reduction of injective matrices."
    M.consolidate()
    return (M, V, W)

