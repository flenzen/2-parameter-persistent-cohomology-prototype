from functools import partial
import pytest, numpy as np, functools as ft
from matrices import GM
from sparse import HeapMatrix, ListMatrix
from minimization_routines import minimize_by_columns, minimize_by_rows, minimize_ptr

@pytest.fixture(params=[HeapMatrix, ListMatrix])
def minimization_problem(request):
    matrix_type = request.param
    instance = GM(
        matrix_type(np.array([
            [1,1,0,1,1,0,0,1],
            [0,0,1,1,1,0,0,1],
            [0,1,0,1,1,1,0,0],
            [0,0,0,0,1,1,1,0],
            [0,0,0,0,0,0,1,0],
            [0,0,0,0,0,0,0,1],
            [0,0,0,0,0,0,1,1]
        ])),
        np.array([ [0,0],[0,1],[1,0],[1,1],[1,1],[2,1],[1,2] ]),
        np.array([ [1,0],[1,0],[0,1],[1,1],[1,1],[2,1],[1,2],[2,2] ])
    )
    result = GM(
        matrix_type(np.array([
            [1,0,1,1],
            [0,0,0,1],
            [0,0,0,1]
        ])),
        np.array([ [0,0],[1,1],[2,1]]),
        np.array([ [1,0],[1,1],[2,1],[2,2] ])
    )
    non_local_rows = [0,4,5]
    non_local_columns = [0,3,5,7]
    return instance, (result, non_local_rows, non_local_columns)

@pytest.mark.parametrize("minimization_function", [
    minimize_by_columns, 
    minimize_ptr, 
    ft.partial(minimize_by_rows, method=minimize_by_columns),
    ft.partial(minimize_by_rows, method=minimize_ptr),
])
def test_minimization_function(minimization_function, minimization_problem):
    instance, (expected_result, expected_nlr, expected_nlc) = minimization_problem
    result, nlr, nlc = minimization_function(instance)
    assert result == expected_result \
        and np.array_equal(nlr, expected_nlr) \
        and np.array_equal(nlc, expected_nlc)

#TODO test chunk