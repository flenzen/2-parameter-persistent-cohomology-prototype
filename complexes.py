""" Combinatorial number system """
from math import comb, inf
import collections.abc as abc, numpy as np

from sparse import HeapMatrix
from matrices import GM, leq_colex, leq_tot, join
from utilities import ProgressInfoIterator, colex_keys, is_colex_sorted, lex_keys
import itertools as it
import random
binom = comb

Bigrade  = tuple[int,int]
Bigrades = list[Bigrade]
Simplices = list[int]
GradedComplex = abc.Iterable[tuple[Simplices, Bigrades]]

def rank(C):
    """ return the rank of the tuple `C`. `C` is assumed sorted ascendingly;
        if this is not the case, use `ranku` instead. """
    return sum(binom(c, k+1) for k, c in enumerate(C))
def ranku(C):
    """ return the rank of the tuple `C`. `C` is not assumed sorted. """
    return sum(binom(c, k+1) for k, c in enumerate(sorted(C)))
def unrank(k, N):
    """ Returns the `k`+1-tuple ranked `N`. """
    C = [None] * (k+1)
    while k >= 0:
        c = 0
        while binom(c+1, k+1) <= N: c += 1
        C[k] = c
        N -= binom(c, k+1)
        k -= 1
    return C
def cofaces(k, N, m=None):
    """ Generator that yields the cofaces of the `k`+1-tuple ranked `N`.
        If `m` is not `None`, generation terminates if the cofaces needs
        an `m`-th vertex. It is assumed that `N` needs less than `m`
        vertices in this case. """
    l = k
    C = [None] * (k+1) # Store the vertices
    L = 0              # Store sum_{i=0}^k binom(C[k], k+2)
    while k >= 0:
        c = 0
        while binom(c+1, k+1) <= N: c += 1
        C[k] = c
        N -= binom(c, k+1)
        L += binom(c, k+2)
        k -= 1
    k = 0              # The index such that C[k-1] < i < C[k]
    i = 0              # The additional vertex
    assert N==0
    while m is None or i < m:
        if k <= l and i == C[k]:
            L -= binom(C[k], k+2)
            N += binom(C[k], k+1)
            k += 1
        else:
            r = L + binom(i, k+1) + N
            yield r
        i += 1
def faces(k, N):
    """ Generator that yields the faces of the `k`+1-tuple ranked `N`. """
    L = 0
    while k >= 0:
        c = 0
        while binom(c+1, k+1) <= N: c += 1
        N -= binom(c, k+1)
        yield N + L
        L += binom(c, k)
        k -= 1

def random_2d_clique_complex(no_of_generators: list[int], max_grade: int = 5, max_dimen = 3):
    v = no_of_generators[0]
    d = 0
    simplices = []
    grades = []
    simplices.append(list(range(v)))
    grades.append([(0, random.randint(0, max_grade)) for _ in range(v)])  # random bi-degrees
    # generate random edges
    d         += 1
    simplices.append([])
    grades  .append([])
    max_rank = sum(binom(v - 1 - d + i, i + 1) for i in range(0, d+1))
    for s in sorted(random.sample(range(max_rank+1), no_of_generators[1])):
        min_x, min_y = join(grades[-2][f] for f in faces(d, s)) # join of the face degrees
        grade        = (random.randint(min_x, max_grade), min_y)          # grade of new simplex
        simplices[-1].append(s)
        grades   [-1].append(grade)
    # generate higher dimensional simplices
    while d < max_dimen:
        d += 1
        simplices.append([])
        grades  .append([])
        max_rank = sum(binom(v - 1 - d + i, i + 1) for i in range(0, d+1))
        for simplex in range(max_rank+1):
            try:
                grade = join(grades[-2][simplices[-2].index(f)] for f in faces(d, simplex))
                simplices[-1].append(simplex)
                grades   [-1].append(grade)
            except ValueError:
                pass
    return (simplices, grades)

def boundary_matrices(cplx):
    """ Generator of the boundary matrices of `cplx`, starting with `C1 -> C0`.
        the iterable `graded_complex`
        in ascending dimension, starting with .
        - `cplx`: iterable, that yields, grouped by ascending dimension, a pair (simplices, bigrades) 
          of lists of equal"""
    cplx                      = iter(cplx)
    row_simplices, row_grades = next(cplx) # simplices and grades of prev. dimension
    dim                       = 1
    for simplices, grades in cplx:
        assert is_colex_sorted(grades), "Simplex grades must be colexicographically descendingly sorted."
        # We have to find the row index of each face of a simplex.
        # Not to have a np.where or list.index in every round, build LUT.
        lut = {row_index: i for i, row_index in enumerate(row_simplices)}
        # for each column, iterate over the faces of the column simplex and add items
        # to the respective boundary columns.
        data = [
                [lut[face] for face in faces(dim, simplex)] 
                for simplex in ProgressInfoIterator(simplices, "Boundary matrix")
            ]
        entries = HeapMatrix.from_column_entries(data, (len(row_simplices), len(simplices)))
        yield GM(entries, row_grades, grades)
        # columns become rows as dimension increases
        dim += 1
        row_simplices, row_grades = simplices, grades

def coboundary_matrices(cplx):
    return (M.D(same_entry_type=True) for M in boundary_matrices(cplx))

def euclidean_distance_matrix(points):
    """ Returns the pairwise distances of the points; where `points` is of shape (N, dim)."""
    return np.linalg.norm(points[:,None,:] - points[None,:,:], axis=-1)

def gaussian_density(distance_matrix, sigma):
    """ Returns for each point the density $\sum_{j\neq i} \exp(\frac{\lVert x_i-x_j\rVert^2}{2r^2})$. """
    return np.sum(np.exp( -(distance_matrix**2) / (2*sigma**2) ), axis=1) - 1

def vietoris_rips(points_or_distances, max_distance=inf, max_distance_quantile=1., number_of_edges=-1, min_density=0, min_density_quantile=0., max_dimen=inf):
    """ Generates the density-length bifiltered Vietoris Rips complex.
        Density = number of points within a radius `density_param` of a points.
        Returns: generator that yields pairs (ranked simplices, bigrades)
        in ascending dimension. In each dimension, the simplices are ordered
        colexicographically descending wrt. their bigrades.
        Bigrades are (Rips, density). """
    if points_or_distances.shape[0] == points_or_distances.shape[1]:
        distances   = points_or_distances
    else:
        points      = points_or_distances
        distances   = euclidean_distance_matrix(points)
        
    density_radius  = np.quantile(distances, 0.5)
    densities       = (100*gaussian_density(distances, density_radius)).astype(int)
    min_density     = max(min_density, np.quantile(densities, min_density_quantile))
    mask            = (densities >= min_density).nonzero()[0]
    distances       = distances[mask[:,None], mask[None,:]]
    densities       = densities[mask]
    n_points        = densities.shape[0]
    densities       = np.random.permutation(n_points)
    sort_keys       = np.lexsort(densities[None,:])
    simplices       = np.arange(n_points)[sort_keys]
    # want to give colex output, so flip the two grade coordinates.
    bigrades        = np.flip(np.vstack((densities[sort_keys], np.zeros((n_points)))).T, axis=1).astype(int)
    print(f"In dimension 0: {n_points: 4} points.")
    yield simplices, bigrades
    # generate pairwise distances matrix and select edges of length below threshold
    edges           = np.triu_indices(n_points, 1)
    distances       = distances[edges]
    max_distance    = min(
                            max_distance, 
                            np.quantile(distances, max_distance_quantile),
                            np.sort(distances)[min(number_of_edges, distances.shape[0])]
                        )
    mask            = distances <= max_distance
    edges           = np.array(edges)[:, mask] # 2 x N array
    lengths         = (1000*distances[mask]).astype(int)

    connected_points = []
    connecting_edges = []
    i1 = 0
    u1 = edges[0, 0]
    for i, u in enumerate(edges[0, 1:], 1):
        if u == u1: 
            continue
        assert u - u1 == 1
        connected_points.append(edges[1, i1:i])
        connecting_edges.append(i1)
        u1, i1 = u, i
    connected_points.append(edges[1, i1:])
    connecting_edges.append(i1)
    connected_points.append(edges[-1:-1])
    connecting_edges.append(len(edges))
    
    def intersect1d(*arrays):
        """ Intersection of arrays. 
            Returns intersection and indices of the intersection in the arrays. """
        a0 = arrays[0]
        i0 = np.arange(len(a0))[None, :]
        for a in arrays[1:]:
            a0, ind_a0, ind_a = np.intersect1d(a0, a, True, True)
            i0 = np.vstack((i0[:,ind_a0], ind_a))
        return a0, i0

    def cofacets(bigrade_simplex):
        # return format:
        # line  0: apexes
        # lines 1: indices from the edges from the i+1st simplex to apex
        dens, diam, s   = bigrade_simplex[0], bigrade_simplex[1], bigrade_simplex[2:].astype(int)
        # find the common neighbors of the points of s
        neighbors       = [connected_points[p] for p in s]
        common_neighbors, indices = intersect1d(*neighbors)
        for i, p in enumerate(s):
            indices[i, :] += connecting_edges[p]
 
        # lengths of the edges from the vertices to the apexes

        new_diameters = np.maximum(diam, np.max(lengths[indices], axis=0))
        new_densities = np.maximum(dens, densities[common_neighbors])
        return np.vstack((
            new_densities,
            new_diameters,
            s[:,None].repeat(len(common_neighbors), axis=1),  # copies of s, one for each common neighbor
            common_neighbors                            # the apexes
        ))
    # Output edges and higher dimensional simplices. `bigraded_simplices` has simplices as columns.
    # In dimension d, the 0th row contains the densities, the 1st the diameters, and the remaining
    # d rows the vertices.
    d               = 1
    bigraded_simplices  = np.vstack((np.max(densities[edges], axis=0), lengths, edges))
    if not (bigraded_simplices.shape[1] != 0 and d < max_dimen):
        return
    while True:
        # sort new simplices lexicographically. lexsort sorts according to last row first;
        # therefore, we use flip to reverse the order of rows. 
        sort_keys           = np.lexsort(np.flip(bigraded_simplices, axis=0))
        bigraded_simplices  = bigraded_simplices[:,sort_keys]
        # convert simplices into combinatorial number system.
        simplices           = np.apply_along_axis(rank, 0, bigraded_simplices[2:])
        # want to give colex output, so flip the two grade coordinates.
        bigrades            = np.flip(bigraded_simplices[:2].T, axis=1)
        print(f"In dimension {d: 2}: {simplices.shape[0]: 4} simplices ({simplices.size / binom(n_points, d+1):.0%} of full complex).")
        yield simplices, bigrades
        # compute d+1 dimensional simplices: compute cofacets for each d-simplex and concatenate.
        if not (bigraded_simplices.shape[1] != 0 and d < max_dimen):
            return
        bigraded_simplices = np.hstack(tuple(map(cofacets, ProgressInfoIterator(bigraded_simplices.T, "New simplices"))))
        d = d+1

def transpose_grading(cplx):
    for simplices, bigrades in cplx:
        bigrades  = np.flip(bigrades, axis=1)
        sort_keys = colex_keys(bigrades)
        bigrades  = bigrades[sort_keys]
        simplices = simplices[sort_keys]
        yield(simplices, bigrades)

def random_sphere(sample_size, ambient_dimension, vertical_noise=0):
    ''' Uniform point sample on a unit sphere with normal distributed vertical offset. '''
    points = np.zeros((0, ambient_dimension))
    while points.shape[0] < sample_size:
        new_sample_size = sample_size-points.shape[0]
        new_points = np.random.random_sample(size=(new_sample_size, ambient_dimension)) * 2 - 1
        norms      = np.linalg.norm(new_points, axis=1)
        selection  = norms > 0.1
        points     = np.vstack((points, new_points[selection,:] / norms[selection, None]))
    noise = np.random.normal(0, vertical_noise, sample_size)
    points = points + noise[:, None] *points
    return points

def random_clifford_torus(sample_size, n=2, vertical_noise=0):
    ''' Uniform point sample from the flat (Clifford) torus in R^{2n}. '''
    return np.hstack([random_sphere(sample_size, 2, vertical_noise) for _ in range(n)])

def random_standard_torus(sample_size, R=3, vertical_noise=0):
    ''' Uniform point sample from the standard torus in R^3. '''
    r = 1
    assert R > r
    points = np.zeros((0, 2))
    while points.shape[0] < sample_size:
        size = sample_size - points.shape[0]
        sample = np.random.random_sample((size, 3)) * np.array([[2*np.pi, 2*np.pi, 1]])
        selection  = sample[:,2] <= (R+r*np.cos(sample[:,1]))/(r+R)
        points = np.vstack((points, sample[selection, :2]))
    r_noise = np.random.normal(r, vertical_noise, sample_size)
    cartesian  = np.stack((
        (R + r_noise*np.cos(points[:,1])) * np.cos(points[:,0]), 
        (R + r_noise*np.cos(points[:,1])) * np.sin(points[:,0]), 
        r_noise*np.sin(points[:,1])
    )).T + np.random.normal()
    return cartesian

from scipy.stats import special_ortho_group, ortho_group
def random_orthogonal_matrices(sample_size, N, noise=0):
    ''' Uniform point sample from O(N). '''
    points = np.vstack([ortho_group.rvs(N).flatten() for _ in range(sample_size)])
    points += np.random.normal(0, noise, points.shape)
    return points

def random_SO_matrices(sample_size, N, noise=0):
    ''' Uniform point sample from SO(N). '''
    points = np.vstack([special_ortho_group.rvs(N).flatten() for _ in range(sample_size)])
    points += np.random.normal(0, noise, points.shape)
    return points

def random_annulus(sample_size, dimension, thickness, density_ratio):
    samples_inner = sample_size / (1 + ((1+thickness)**2 - 1) * density_ratio)
    samples = []
    while len(samples) < samples_inner:
        r = 2 * np.random.random(dimension) - 1
        if np.linalg.norm(r) < 1:
            samples.append(r)
    while len(samples) < sample_size:
        r = (2 * np.random.random(dimension) - 1) * (1 + thickness)
        if 1 <= np.linalg.norm(r) <= 1 + thickness:
            samples.append(r)
    samples = np.random.permutation(samples)
    return samples

def is_graded_complex(cplx) -> bool:
    """ Check if the input `cplx = [(simplices, grades)]` represents a graded complex.
        `cplx` is supposed to be understood as subcomplex of some full N-simplex,
        and the simplices of `cplx` are to be provided encoded in the combinatorial
        number system. """
    i = iter(cplx)
    ld_simplices, ld_grades = next(i)
    d = 1
    for simplices, grades in i:
        for t, t_gr in zip(simplices, grades):
            for r in faces(d, t):
                r_gr = ld_grades[list(ld_simplices).index(r)]
                if not leq_tot(r_gr, t_gr):
                    return False
        ld_simplices, ld_grades = simplices, grades
        d += 1
    return True
