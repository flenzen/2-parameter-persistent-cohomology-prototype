#!/usr/bin/env python3 
from __future__ import annotations
import builtins
import heapq, copy, numpy as np
from math import inf
from matrices import GM, leq_colex, leq_tot, join
import warnings
from sparse import HeapMatrix, LocMatrix, LorMatrix
from utilities import is_colex_sorted
from builtins import print
def kernel_2d_sparse(M: GM, ker=True, inverse=False, mgs=False, ker_mgs=False):
    """ Computes a graded matrix representing the embedding of ker `M`.
        `M`: The matrix to be reduced.  `M` must have columns in colex-ascending order
            for the returned kernel inclusion matrix to have unique pivots.
        Returns: A tuple containing the following `GM` matrices, in that order:
            - if `kernel` is true, the kernel inclusion matrix with columns in colex order,
            - if `inverse` is true, its inverse,
            - if `mgs` is true, a minimal generating system of the image of `M`,
              with columns in lex order.
            - if `ker_mgs` is true, compute the kernel in terms of the minimal generating system."""
    A, g = M.entries, M.col_grades
    #assert is_colex_sorted(g)
    p = np.full(M.entries.shape[0], -1)         # assignment row -> column with that pivot row.
    if ker:
        kernel_cols = []                        # V[:,kernel_cols] eventually represents kernel inclusion.
        kernel_grades = []                      # grades of these columns
        V = HeapMatrix.eye(M.entries.shape[1])   # Represents col operations on M.
    if inverse:
        W = LorMatrix.eye(M.entries.shape[1])   # V^{-1}
    if mgs:
        image_cols    = []                      # Column contents of a min generating system if im M
        image_grades  = []                      # grades of these generators
    if ker_mgs:
        V_mgs = HeapMatrix.eye(0)
        mgs_indices = [None for _ in range(A.shape[1])]
        kernel_mgs_cols = []
        kernel_mgs_grades = []
    
    # priority queue Q of grades and columns to visit.  Primary key is grade, which is put in lex order.
    Q = [(tuple(grade), j) for j, grade in enumerate(M.col_grades)]
    heapq.heapify(Q)
    no_kernel_cols = 0
    print()
    x = np.full(M.shape[1], 0)
    while len(Q):
        #print("\33[2K\r", len(Q), end='')
        z, j = heapq.heappop(Q)
        x[j] += 1
        while (i := A.pivot(j)) != -1:
            if (k := p[i]) == -1:
                p[i] = j
                break
            elif not(leq_tot(g[k], z) and k < j):
                heapq.heappush(Q, (join(g[k], z), k))
                p[i] = j
                break
            else:
                A.column_operation(k, j)
                if ker:
                    V.column_operation(k, j)
                if ker_mgs and mgs_indices[j] is not None:
                    V_mgs.column_operation(mgs_indices[k], mgs_indices[j])
                if inverse:
                    W.row_operation(j, k)
        # if loop terminates, have found a kernel generator.
        else: 
            if ker:
                kernel_cols.append(j)
                kernel_grades.append(z)
            if ker_mgs and mgs_indices[j] is not None:
                kernel_mgs_cols.append(mgs_indices[j])
                kernel_mgs_grades.append(z)
            no_kernel_cols += 1
            continue
        # if the linear combination at the column stores now is valid at z, 
        # it adds a generator to the image.
        if mgs and np.array_equal(z, g[j]):
            image_cols.append(copy.deepcopy(A[:,j]))
            image_grades.append(z)
        if ker_mgs and np.array_equal(z, g[j]):
            V_mgs.extend_rows(r := (V_mgs.shape[0]+1))
            V_mgs.extend(HeapMatrix.eye(r, 1, -(r-1)))
            mgs_indices[j] = r-1
    result = []
    print("\33[2K", end='')
    if ker:
        kernel_grades = np.array(kernel_grades)
        K = GM(V[:, kernel_cols], M.col_grades, kernel_grades)
        result.append(K)
    if inverse:
        L = GM(W[kernel_cols, :], kernel_grades, M.col_grades, not_well_defined=True)
        result.append(L)
    if mgs:
        if len(image_cols) > 0:
            I = GM(np.hstack(image_cols), M.row_grades, image_grades)
        else:
            I = GM(HeapMatrix.zeros((M.shape[0], 0)), M.row_grades, image_grades)
        result.append(I)
    if ker_mgs:
        K_mgs = GM(V_mgs[:, kernel_mgs_cols], image_grades, kernel_mgs_grades)
        result.append(K_mgs)
    return result[0] if len(result) == 1 else result

def homology_degrees(cohomology_degrees):
    """ Converts degrees of 2-, 1 and 0-syzygies of a free cohomology resolution
        to the degrees of 0-, 1- and 2-syzygies of the respective free cohomology
        resolution.""" 
    return 1-cohomology_degrees[np.logical_and(-inf < cohomology_degrees[:,0], -inf < cohomology_degrees[:,1])]
def pretty_print_grades(grades):
    return ", ".join(f"({x:.3}, {y:.3})" for x, y in grades)
