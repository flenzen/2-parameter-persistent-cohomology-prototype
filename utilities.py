import time, itertools as it, functools as ft, operator as op, line_profiler, numpy as np, builtins, contextlib, signal
neg_inf = np.iinfo(np.dtype(int)).min+1
pos_inf = np.iinfo(np.dtype(int)).max
# Remove all tab stops and set a single tab stop at 60.
print(" " * 60 + "\x1b[3g\x1bH\r")
class PrintingContext(contextlib.ContextDecorator):
    """ Context wrapper that provides a custom (global) `print` function to its
        contents. Can be used as decorator to wrap an entire function in a printing
        context. Entering `PrintingContext`s changes the behaviour of the `print` function."""
    current_context = None
    def __init__(self):
        pass
    def __enter__(self):
        self.context = PrintingContext.current_context
        PrintingContext.current_context = self
        return self
    def __exit__(self, *_):
        PrintingContext.current_context = self.context
    def print(self, *args, **kwargs):
        self.context.print(*args, **kwargs)

def print(*args, **kwargs):
    """ Drop-in replacement for `builtins.print`. Invokes the current `PrintingContext`'s
        `print`-method to print. """
    PrintingContext.current_context.print(*args, **kwargs)

class StandardPrintingContext(PrintingContext):
    def __init__(self):
        super().__init__()
    def print(self, *args, **kwargs):
        builtins.print(*args, **kwargs)

PrintingContext.current_context = StandardPrintingContext()

class Indentation(PrintingContext):
    """ Adds a level of indendation to all calls to `print` inside the context. """
    def __init__(self, message=""):
        super().__init__()
        self.message = message
        self.on_new_line = True
    def __enter__(self):
        super().__enter__()
        self.context.print(self.message)
        return self
    def print(self, *args, **kwargs):
        if self.on_new_line:
            super().print("│ ", end='')
        super().print(*args, **kwargs)
        if kwargs.get('end', None) is not None:
            self.on_new_line = False
        else:
            self.on_new_line = True

class Duration(Indentation):
    """ Prints the duration elapsed between entering end exiting the context upon exiting. """
    def __init__(self, message=""):
        super().__init__(message)
    def __enter__(self):
        super().__enter__()
        self.start = time.time()
        return self
    def __exit__(self, *_):
        self.duration = time.time() - self.start
        super().__exit__(self, *_)
        self.context.print(f"└ {self.duration:.2f}s")

class Silence(PrintingContext):
    """ Supresses any printing from code inside the context."""
    def __init__(self):
        super().__init__()
    def print(self, *args, **kwargs):
        pass

class SDuration(Silence):
    """ Prints the duration elapsed between entering end exiting the context.
        Supresses any printing from code inside the context."""
    def __init__(self, message=''):
        super().__init__()
        self.message = message
    def __enter__(self):
        print(self.message + '\t', end='', flush=True)
        super().__enter__()
        self.start_time = time.time()
        return self
    def __exit__(self, *_):
        self.duration = time.time() - self.start_time
        super().__exit__(*_)
        print(f"\x1b[K{self.duration:.3f}s")

class ProgressInfoIterator:
    """ Iterator that can be wrapped around an iterable. Registers the SIGINFO signal.
        Upon receiving SIGINFO, reports the progress in percent by which the wrapped
        iterable has been exhausted."""
    def __init__(self, iterable, name="", length=None):
        self.iterator = iter(iterable)
        self.length = length if length is not None else len(iterable)
        self.count    = 0
        self.name = name
    def __iter__(self):
        print(self.name + "\t" + f"   I", end='', flush=True)
        self.start_time = time.time()
        self.iterating = True
        self.original_handler = signal.getsignal(signal.SIGINFO)
        signal.signal(signal.SIGINFO, self.handle_siginfo)
        return self
    def __next__(self):
        try:
            result = next(self.iterator)
            self.count += 1
            return result
        except StopIteration:
            if self.iterating:
                print(f"\b\b\b\b{time.time() - self.start_time: >6.2f}s")
                signal.signal(signal.SIGINFO, self.original_handler)
                self.iterating = False
            raise
    def handle_siginfo(self, signal, stackframe):
        print(f"\b\b\b\b{self.count / self.length: >4.0%}", flush=True, end='')

class ProgressIterator:
    """ Iterator that can be wrapped around an iterable. Constantly reports
        the progress in percent by which the wrapped iterable has been exhausted."""
    def __init__(self, iterable, name="", length=None):
        self.iterator = iter(iterable)
        self.length = length if length is not None else len(iterable)
        self.count    = 0
        self.skip = max(1, self.length // 100)
        self.name = name
        self.rotation = 0
    def __iter__(self):
        print(self.name + "\t" + f"{0: >4.0%} |", end='', flush=True)
        self.start_time = time.time()
        self.iterating = True
        return self
    def __next__(self):
        try:
            result = next(self.iterator)
            self.count += 1
            if self.count % self.skip == 0:
                self.rotation = (self.rotation + 1) % 4
            print("\b"*6 + f"{self.count / self.length: >4.0%} " + "–\\|/"[self.rotation], flush=True, end='') # ⌜⌝⌟⌞
            return result
        except StopIteration:
            if self.iterating:
                print("\b"*6 + f"{time.time() - self.start_time: >6.2f}s")
                self.iterating = False
            raise

class Profiler:
    """ Context manager that enables a new `LineProfiler` instance upon `__enter__`
        that profiles the functions passed to `Profiler`'s `__init__` routine.
        Prints the profile upon exit."""
    def __init__(self, *functions):
        self.profiler = line_profiler.LineProfiler(*functions)
    def __enter__(self):
        self.profiler.enable()
    def __exit__(self, *_):
        self.profiler.disable()
        self.profiler.print_stats()

def all_equal(iterable, eq = op.eq):
    i = iter(iterable)
    first = next(i, None)
    return first is None or all(eq(first, item) for item in i)

def inverse_permutation(permutation: np.ndarray):
    inverse = np.empty_like(permutation)
    inverse[permutation] = np.arange(len(permutation))
    return inverse

def integer_retraction(array, domain):
    """ If `array` is an injective integer mapping `range(len(indices)) ->  range(domain)`,
        then `integer_retraction` returns the left inverse mapping. The elements of 
        range(domain) - im(array) are mappined to -1. """
    inverse = np.full(domain, -1)
    inverse[array] = np.arange(len(array))
    return inverse

def lex_keys(grades):
    return np.lexsort(grades[:,::-1].T)

def colex_keys(grades):
    return np.lexsort(grades.T)

def lex_sorted(grades):
    return grades[lex_keys(grades)]

def colex_sorted(grades):
    return grades[colex_keys(grades)]

def is_lex_sorted(a):
    return np.array_equal(lex_keys(a), np.arange(len(a)))

def is_colex_sorted(a):
    return np.array_equal(colex_keys(a), np.arange(len(a)))

def is_non_decreasingly_sorted(array):
    """ Returns true if the sorting of the array is compatible with the partial order; i.e.,
        if no po-smaller element comes later in the array. """
    return is_lex_sorted(array) or is_colex_sorted(array)
    # return all(not np.any(np.all(a < array[:i], axis=1)) for i, a in enumerate(array))


def mod2set(ls):
    """ Converts `ls` into a set, where only elements of `ls` occurring an odd number of times are included."""
    u, c = np.unique(ls, return_counts=True)
    return set(u[c % 2])

def is_strictly_sorted(ls):
    return np.all(np.diff(ls) > 0)

def has_duplicates(ls):
    return np.any(np.unique(ls, return_counts=True)[1] != 1)

def sdiv(n, d):
    return n/d if d!= 0 else float('Nan')

def to_top(grades):
    """ Shifts a bigrade to the 'top' infinity (in the cohomological sense). """
    grades = np.copy(grades)
    grades[:, 1] = neg_inf
    return grades

def to_right(grades):
    """ Shifts a bigrade to the 'right' infinity (in the cohomological sense). """
    grades = np.copy(grades)
    grades[:, 0] = neg_inf
    return grades